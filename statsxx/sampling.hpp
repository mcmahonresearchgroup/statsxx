#ifndef STATSxx_SAMPLING_HPP
#define STATSxx_SAMPLING_HPP

// STL
#include <utility> // std::pair<>
#include <vector>  // std::vector<>


namespace statsxx
{
    namespace sampling
    {
        template<typename T>
        std::vector<T> sample_wo_replacement(
                                             const std::vector<T> &x,
                                             const int n
                                             );

        template<typename T>
        std::vector<T> sample_w_replacement(const std::vector<T> &x);


        std::pair<
                  double, // d2n
                  int     // m
                  > sample_size_multinomial(
                                            const double alpha
                                            );

        std::pair<
                 double, // alpha
                 int     // m
                 > significance_multinomial(
                                            const double d2n
                                            );

        std::pair<
                 double, // u
                 int     // m
                 > uncertainty_multinomial(
                                           const double alpha,
                                           // -----
                                           const int    n
                                           );

        int sample_size_m(
                          const double alpha
                          );
    }
}

#include "statsxx/sampling/sample_wo_replacement.tpp"
#include "statsxx/sampling/sample_w_replacement.tpp"

#include "statsxx/sampling/sample_size_multinomial.cpp"
#include "statsxx/sampling/significance_multinomial.cpp"
#include "statsxx/sampling/uncertainty_multinomial.cpp"
#include "statsxx/sampling/sample_size_m.cpp"


#endif // STATSxx_SAMPLING_HPP
