#ifndef STATSxx_EDA_HPP
#define STATSxx_EDA_HPP

// STL
#include <utility>                    // std::pair<>
#include <vector>                     // std::vector<>

// jScience
#include "jScience/linalg/Matrix.hpp" // Matrix<>, Vector<>


std::pair<
          Matrix<double>,     // VT
          std::vector<double> // S
          > PCA(
                const Matrix<double> &X
                );
#include "statsxx/EDA/PCA.cpp"


std::vector<Vector<double>> LDA(
                                const int                                       nC,
                                // -----
                                const int                                       nd,
                                // =====
                                const std::vector<std::vector<Vector<double>>> &x
                                );
#include "statsxx/EDA/LDA.cpp"

#endif
