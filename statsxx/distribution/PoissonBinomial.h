#pragma once

// C++SL
#include <utility> // std::pair<>


// DESC: Confidence interval for the Poisson-Binomial distribution.
//
// INPUT:
//
//    alpha : one-sided confidence parameter; e.g., 0.025 = 95% CI
//
// NOTE: From: https://arxiv.org/pdf/2212.12558.pdf
//
std::pair<double, double> ci_PoissonBinomial(const int n, const int nT, const double alpha);
#include "tpp/ci_PoissonBinomial.cpp"
