/*
 Copyright 2013--Present JMM_PROGNAME
 
 This file is distributed under the terms of the JMM_PROGNAME License.
 
 You should have received a copy of the JMM_PROGNAME License.
 If not, see <JMM_PROGNAME WEBSITE>.
*/
// CREATED    : 9/11/2015
// LAST UPDATE: 9/11/2015

#ifndef STATSxx_DISTRIBUTION_BINOMIAL_HPP
#define STATSxx_DISTRIBUTION_BINOMIAL_HPP


// jScience
#include "jScience/linalg/Vector.hpp" // Vector<>


namespace distribution
{
    // binomial distribution:
    template<typename T>
    T binomial(int n, double p);
    
    template<typename T>
    Vector<T> binomial(int n, const Vector<double> &v);
}

#include "./tpp/binomial.tpp"


#endif
