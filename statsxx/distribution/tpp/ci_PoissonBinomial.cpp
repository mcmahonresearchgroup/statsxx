// C++SL
#include <string>    // std::string
#include <utility>   // std::pair<>
#include <stdexcept> // std::invalid_argument()
// Boost
#include <boost/math/special_functions/beta.hpp> // boost::math::ibeta(), ::ibeta_inv()


static double qg_alpha(const int nT, const int n, const double alpha);


inline std::pair<double, double> ci_PoissonBinomial(const int n, const int nT, const double alpha)
{
    // Theorem 1
    // Eqs. (46) and (47)
    return { qg_alpha(nT, n, alpha), (double{ 1 } - qg_alpha((n - nT), n, alpha)) };
}


static double qg_alpha(const int nT, const int n, const double alpha)
{
    double _q;

    if (nT == 0)
    {
        _q = double{ 0 };
    }
    else if (nT > 0)
    {
        int n1T = n - nT; // (n * (1 - \bar{T})) the number of Bernoulli games not won

        double alphas = boost::math::ibeta(static_cast<double>(nT), static_cast<double>(n1T + 1), (static_cast<double>(nT - 1) / static_cast<double>(n))); // alpha*

        if ((alpha >= alphas) && (alpha <= double{ 1 })) // NOTE: The following two bounds checks on alpha overlap at (alpha = alphas).
        {
            double Tbar = static_cast<double>(nT) / static_cast<double>(n);

            _q = Tbar - (double{ 1 } - alpha) / (n * (double{ 1 } - alphas));
        }
        else if ((alpha >= double{ 0 }) && (alpha <= alphas))
        {
            double Iinv = boost::math::ibeta_inv(static_cast<double>(nT), static_cast<double>(n1T + 1), alpha);

            _q = Iinv;
        }
    }
    else
    {
        throw std::invalid_argument("qg_alpha()" + std::string(": ") + "nT < 0");
    }

    return _q;
}
