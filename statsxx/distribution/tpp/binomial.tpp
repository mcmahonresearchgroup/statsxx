/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of FDTD++.
 
 FDTD++ is proprietary software: you can use it and/or modify it
 under the terms of the Algorithms in Motion License as published by
 Algorithms in Motion LLC, either version 1 of the License, or (at your
 option) any later version.
 
 FDTD++ is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 Algorithms in Motion License for more details.
 
 You should have received a copy of the Algorithms in Motion License
 along with FDTD++. If not, see <http://www.aimotionllc.com/licenses/>.
*/
// CREATED    : 9/11/2015
// LAST UPDATE: 9/11/2015


// STL
#include <random> // std::random_device, std::mt19937, std::binomial_distribution<>

// jScience
#include "jScience/linalg/Vector.hpp" // Vector<> 


// binomial distribution:
template<typename T>
T distribution::binomial(int n, double p)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    //    std::default_random_engine generator;
    std::binomial_distribution<> d(n,p);
    
    return static_cast<T>(d(gen));
}

// binomial distribution wrapper (for a Vector<>)
template<typename T>
Vector<T> distribution::binomial(int n, const Vector<double> &v)
{
    Vector<T> v_ret(v.size());
    
    for(auto i = 0; i < v.size(); ++i)
    {
        v_ret(i) = binomial<T>(n, v(i));
    }
    
    return v_ret;
}
