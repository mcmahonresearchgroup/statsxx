// STL
#include <vector>       // std::vector<>
// jScience
#include "jrandnum.hpp" // rand_num_uniform_Mersenne_twister()


// DESC: Samples a std::vector<> with replacement.
//
template<typename T>
std::vector<T> statsxx::sampling::sample_w_replacement(const std::vector<T> &x)
{
    std::vector<T> xs;

    for(auto i = 0; i < x.size(); ++i)
    {
        int r = rand_num_uniform_Mersenne_twister(0, static_cast<int>(x.size() - 1));

        xs.push_back(x[r]);
    }

    return xs;
}
