
// STL
#include <vector>  // std::vector<>

// jScience
#include "jScience/stats/data.hpp" // shuffle_data()


// DESC: Samples a vector x with replacement
template<typename T>
std::vector<T> statsxx::sampling::sample_wo_replacement(
                                                        const std::vector<T> &x,
                                                        const int n
                                                        )
{
    // FIXME: maybe it would be better here to assign xsmpl to x, shuffle it, and erase latter elements
    
    std::vector<T> xsmpl;
    
    std::vector<T> x_shuffled = x;
    shuffle_data(x_shuffled);

    for(int i = 0; i < n; ++i)
    {
        xsmpl.push_back(x_shuffled[i]);
    }
    
    return xsmpl;
}



