// << jmm: what is needed is to be able to pass any type of RNG that one wants to this to generate the random numbers for resampling -- stats++ needs a framework for this >>

#include "statsxx/sampling/bootstrap/Bootstrap.hpp"

// STL
#include <vector>     // std::vector<>

// jScience
#include "jrandnum.hpp" // rand_num_uniform_Mersenne_twister()


inline std::vector<double> sampling::Bootstrap::resample(const std::vector<double> &x)
{
    std::vector<double> xrs;

    for(auto i = 0; i < x.size(); ++i)
    {
        int r = rand_num_uniform_Mersenne_twister(0, static_cast<int>(x.size()-1));

        xrs.push_back(x[r]);
    }

    return xrs;
}

inline std::vector<int> sampling::Bootstrap::resample(const int nx)
{
    std::vector<int> ors;

    for(auto i = 0; i < nx; ++i)
    {
        int r = rand_num_uniform_Mersenne_twister(0, (nx-1));

        ors.push_back(r);
    }

    return ors;
}
