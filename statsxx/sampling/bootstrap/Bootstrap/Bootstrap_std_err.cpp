#include "statsxx/sampling/bootstrap/Bootstrap.hpp"

// STL
#include <stdexcept>  // std::runtime_error()

// jScience
#include "jstats.hpp" // stddev()


inline double sampling::Bootstrap::std_err()
{
    if(this->resampled)
    {
        return stddev(this->samples);
    }
    else
    {
        throw std::runtime_error("error in sampling::Bootstrap::std_err(): bootstrap not initialized with data");
    }
}

