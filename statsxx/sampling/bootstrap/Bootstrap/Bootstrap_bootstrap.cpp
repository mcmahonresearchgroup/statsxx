#include "statsxx/sampling/bootstrap/Bootstrap.hpp"

// STL
#include <functional> // std::function<>
#include <numeric>    // std::iota()
#include <vector>     // std::vector<>


inline void sampling::Bootstrap::bootstrap(
                                          const std::vector<double> &x,
                                          const int n,
                                          std::function<double(const std::vector<double> &)> f
                                          )
{
    // store the sample estimate of f()
    this->f0 = f(x);

    // resample to get bootstrap estimates of f()
    this->samples.clear();

    for(int i = 0; i < n; ++i)
    {
        this->samples.push_back(f(this->resample(x)));
    }

    this->resampled = true;
}

inline void sampling::Bootstrap::bootstrap(
                                           const int nx,
                                           const int n,
                                           std::function<double(const std::vector<int> &)> f
                                           )
{
    // NOTE: See the NOTEs in ::bootstrap() above.

    std::vector<int> o0(nx);
    std::iota(o0.begin(), o0.end(), 0);

    this->f0 = f(o0);

    this->samples.clear();

    for(int i = 0; i < n; ++i)
    {
        this->samples.push_back(f(this->resample(nx)));
    }

    this->resampled = true;
}

