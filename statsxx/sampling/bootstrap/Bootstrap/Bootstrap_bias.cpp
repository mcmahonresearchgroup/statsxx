#include "statsxx/sampling/bootstrap/Bootstrap.hpp"

// STL
#include <stdexcept>  // std::runtime_error()
#include <utility>    // std::pair<>, std::make_pair()

// jScience
#include "jstats.hpp" // mean()


inline std::pair<
                 double,
                 double
                 > sampling::Bootstrap::bias()
{
    if(this->resampled)
    {
        double bias = mean(this->samples) - this->f0;
        double fbc = this->f0 - bias;

        return std::make_pair(
                              bias,
                              fbc
                              );
    }
    else
    {
        throw std::runtime_error("error in sampling::Bootstrap::bias(): bootstrap not initialized with data");
    }
}

