#ifndef STATSxx_SAMPLING_BOOTSTRAP_BOOTSTRAP_HPP
#define STATSxx_SAMPLING_BOOTSTRAP_BOOTSTRAP_HPP


// STL
#include <functional> // std::function<>
#include <utility>    // std::pair<>
#include <vector>     // std::vector<>


namespace sampling
{
    //=========================================================
    // Bootstrap
    //=========================================================
    class Bootstrap
    {

    public:

        Bootstrap();
        ~Bootstrap();

        void bootstrap(
                      const std::vector<double> &x,
                      const int n,
                      std::function<double(const std::vector<double> &)> f
                      );

        void bootstrap(
                       const int nx,
                       const int n,
                       std::function<double(const std::vector<int> &)> f
                       );

        double std_err();
        //    std::pair<
        //              double,
        //              double
        //              > conf_int(const double ci);

        std::pair<
                  double,
                  double
                  > bias();

//        std::vector<double> get_samples();

    private:

        bool resampled;

        double f0;
        std::vector<double> samples;

        std::vector<double> resample(const std::vector<double> &x);
        std::vector<int> resample(const int nx);

    };
}

#include "statsxx/sampling/bootstrap/Bootstrap/Bootstrap.cpp"
#include "statsxx/sampling/bootstrap/Bootstrap/Bootstrap_bootstrap.cpp"
#include "statsxx/sampling/bootstrap/Bootstrap/Bootstrap_std_err.cpp"
#include "statsxx/sampling/bootstrap/Bootstrap/Bootstrap_bias.cpp"
#include "statsxx/sampling/bootstrap/Bootstrap/Bootstrap_resample.cpp"


#endif
