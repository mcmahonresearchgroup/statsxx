/*
 Copyright 2013--Present JMM_PROGNAME
 
 This file is distributed under the terms of the JMM_PROGNAME License.
 
 You should have received a copy of the JMM_PROGNAME License.
 If not, see <JMM_PROGNAME WEBSITE>.
*/
// CREATED    : 10/17/2015
// LAST UPDATE: 10/17/2015


// STL
#include <functional> // std::function<>
#include <vector>     // std::vector<>


//========================================================================
//========================================================================
//
// NAME: double sampling::wrap(
//                             const std::vector<double> &x0,
//                             const std::vector<double> &y0,
//                             const std::vector<int> &o,
//                             std::function<double(const std::vector<double> &, const std::vector<double> &)> f
//                             )
//
// DESC: Wraps a function for bootstrap (which tries to call just double f(const std::vector<int> &)).
//
//========================================================================
//========================================================================
inline double sampling::wrap(
                             const std::vector<double> &x0,
                             const std::vector<double> &y0,
                             const std::vector<int> &o,
                             std::function<double(const std::vector<double> &, const std::vector<double> &)> f
                             )
{
    std::vector<double> x;
    std::vector<double> y;
    std::tie(
             x,
             y
             ) = sampling::convert(
                                   x0,
                                   y0,
                                   o
                                   );
    
    return f(
             x,
             y
             );
}
