/*
 Copyright 2013--Present JMM_PROGNAME
 
 This file is distributed under the terms of the JMM_PROGNAME License.
 
 You should have received a copy of the JMM_PROGNAME License.
 If not, see <JMM_PROGNAME WEBSITE>.
*/
// CREATED    : 10/17/2015
// LAST UPDATE: 10/17/2015


// STL
#include <utility> // std::pair<>, std::make_pair()
#include <vector>  // std::vector<>


//========================================================================
//========================================================================
//
// NAME: std::pair<
//                 std::vector<double>,
//                 std::vector<double>
//                 > sampling::convert(
//                                     const std::vector<double> &x0,
//                                     const std::vector<double> &y0,
//                                     const std::vector<int> &o
//                                     )
//
// DESC: Samples from the vectors x0 and y0 using o.
//
//========================================================================
//========================================================================
inline std::pair<
                 std::vector<double>,
                 std::vector<double>
                 > sampling::convert(
                                     const std::vector<double> &x0,
                                     const std::vector<double> &y0,
                                     const std::vector<int> &o
                                     )
{
    std::vector<double> x;
    std::vector<double> y;
    
    for(auto &oi : o)
    {
        x.push_back(x0[oi]);
        y.push_back(y0[oi]);
    }
    
    return std::make_pair(
                          x,
                          y
                          );
}
