//
// DESC: Determine the number of nonzero parameters, at which the "worst case" occurs for the a given significance level.
//
// -----
//
// INPUT:
//
//     alpha : significance level
//
// -----
//
// OUTPUT:
//
//     m     : the number of nonzero parameters, at which the "worst case" occurs for the given alpha
//
// -----
//
// NOTE: See:
//
//     S. K. Thompson, "Sample Size for Estimating Multinomial Proportions," The American Statistician 41, 42--46 (1987)
//
// -----
//
inline int statsxx::sampling::sample_size_m(
                                            const double alpha
                                            )
{
    int m;

    // DETERMINE m
    if( (alpha > 0.) && (alpha < 0.0344) )
    {
        m = 2;
    }
    else if( (alpha >= 0.0344) && (alpha < 0.3466) )
    {
        m = 3;
    }
    else if( (alpha >= 0.3466) && (alpha < 0.6311) )
    {
        m = 4;
    }
    else if( (alpha >= 0.6311) && (alpha < 0.8934) )
    {
        m = 5;
    }
    else if( (alpha >= 0.8934) && (alpha < 1) )
    {
        m = 6;
    }

    // RETURN
    return m;
}
