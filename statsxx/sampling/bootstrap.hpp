/*
 Copyright 2013--Present JMM_PROGNAME
 
 This file is distributed under the terms of the JMM_PROGNAME License.
 
 You should have received a copy of the JMM_PROGNAME License.
 If not, see <JMM_PROGNAME WEBSITE>.
*/
// CREATED    : 10/17/2015
// LAST UPDATE: 10/17/2015

#ifndef STATSxx_SAMPLING_BOOTSTRAP_HPP
#define STATSxx_SAMPLING_BOOTSTRAP_HPP


// STL
#include <functional> // std::function<>
#include <utility>    // std::pair<>
#include <vector>     // std::vector<>


namespace sampling
{
    double wrap(
                const std::vector<double> &x0,
                const std::vector<double> &y0,
                const std::vector<int> &o,
                std::function<double(const std::vector<double> &, const std::vector<double> &)> f
                );
    
    std::pair<
              std::vector<double>,
              std::vector<double>
              > convert(
                        const std::vector<double> &x0,
                        const std::vector<double> &y0,
                        const std::vector<int> &o
                        );
}


#include "statsxx/sampling/bootstrap/Bootstrap.hpp" // Bootstrap

#include "statsxx/sampling/bootstrap/wrap.cpp" 
#include "statsxx/sampling/bootstrap/convert.cpp"


#endif
