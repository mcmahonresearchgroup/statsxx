// TODO: I wonder if this (or the main functionality) could be moved over to reading (and writing) Matrix<> objects
// TODO: ... e.g., the << and >> opertors could be overloaded


// STL
#include <fstream> // std::ifstream
#include <sstream> // std::stringstream
#include <string>  // std::string
#include <vector>  // std::vector<>

// jScience
#include "jScience/linalg.hpp"      // Matrix<double>
#include "jScience/stl/istream.hpp" // is_eof()


//
// DESC: Reads a data matrix from a file.
//
inline Matrix<double> read_datafile(
                                    const std::string filename
                                    )
{
    // TODO: HACK: this whole subroutine is basically a hack
    
    // ----- READ DATA AND STORE INTO A 2D VECTOR -----
    std::ifstream ifs(filename);
    
    std::vector<std::vector<double>> X_2D;
    
    while( !is_eof(ifs, true) )
    {
        std::string line;
        std::getline(ifs, line);
        
        std::stringstream ss(line);
        
        std::vector<double> x;
        
        double xi;
        while(ss >> xi)
        {
            x.push_back(xi);
        }
        
        X_2D.push_back(x);
    }
    
    ifs.close();
    
    // ----- CONVERT THE 2D VECTOR TO A Matrix<> -----
    Matrix<double> X(X_2D.size(),X_2D[0].size());
    
    for(auto i = 0; i < X_2D.size(); ++i)
    {
        for(auto j = 0; j < X_2D[i].size(); ++j)
        {
            X(i,j) = X_2D[i][j];
        }
    }
    
    return X;
}



