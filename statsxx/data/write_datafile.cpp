// STL
#include <fstream> // std::ifstream
#include <string>  // std::string

// jScience
#include "jScience/linalg.hpp"      // Matrix<double>


//
// DESC: Writes a data matrix to a file.
//
inline void write_datafile(
                           const Matrix<double> &X,
                           const std::string     filename
                           )
{
    std::ofstream ofs(filename);
    
    for(auto i = 0; i < X.size(0); ++i)
    {
        for(auto j = 0; j < X.size(1); ++j)
        {
            ofs << X(i,j) << " ";
        }
        
        ofs << '\n';
    }
        
    ofs.close();
}
