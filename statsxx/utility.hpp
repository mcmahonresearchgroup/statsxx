#ifndef STATSxx_UTILITY_HPP
#define STATSxx_UTILITY_HPP

// STL
#include <vector> // std::vector<>


namespace statsxx
{
namespace utility
{
    template<typename T>
    std::vector<std::vector<T>> XY(
                                   const std::vector<T>& x,
                                   const std::vector<T>& y
                                   );
}
}

#include "statsxx/utility/XY.cpp"

#endif
