#include "statsxx/machine_learning/DBN.hpp"

// STL
#include <vector> // std::vector<>

// jScience
#include "jScience/linalg.hpp" // Matrix<double>


// using namespace machine_learning;


//
// DESC: Gets the weights of the DBN.
//
inline std::vector<Matrix<double>> machine_learning::DBN::get_W() const
{
    std::vector<Matrix<double>> W;

    for(auto i = 0; i < this->RBM.size(); ++i)
    {
        W.push_back(this->RBM[i].get_W());
    }

    return W;
}
