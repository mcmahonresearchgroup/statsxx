#include "statsxx/machine_learning/DBN.hpp"

// STL
#include <vector> // std::vector<>


// using namespace machine_learning;


//
// DESC: Gets the architecture of the DBN.
//
inline std::vector<int> machine_learning::DBN::architecture() const
{
    std::vector<int> architecture_;

    architecture_.push_back(this->RBM[0].get_nv());

    for(auto i = 0; i < this->RBM.size(); ++i)
    {
        architecture_.push_back(this->RBM[i].get_nh());
    }

    return architecture_;
}
