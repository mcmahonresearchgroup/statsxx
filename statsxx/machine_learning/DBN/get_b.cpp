#include "statsxx/machine_learning/DBN.hpp"

// STL
#include <vector> // std::vector<>

// jScience
#include "jScience/linalg.hpp" // Vector<double>


// using namespace machine_learning;


//
// DESC: Gets the hidden biases of the DBN.
//
inline std::vector<Vector<double>> machine_learning::DBN::get_b() const
{
    std::vector<Vector<double>> b;

    for(auto i = 0; i < this->RBM.size(); ++i)
    {
        b.push_back(this->RBM[i].get_b());
    }

    return b;
}
