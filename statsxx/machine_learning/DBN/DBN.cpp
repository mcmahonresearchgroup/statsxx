#include "statsxx/machine_learning/DBN.hpp"

// STL
#include <vector> // std::vector<>

// jScience
#include "jScience/linalg.hpp" // Matrix<>, Vector<>


// using namespace machine_learning;


inline machine_learning::DBN::DBN() {};

inline machine_learning::DBN::DBN(
                                  const int              nRBM,
                                  // -----
                                  const std::vector<int> nv_,
                                  const std::vector<int> nh_,
                                  // -----
                                  const std::vector<int> vtype_,
                                  const std::vector<int> htype_  
                                  )
{
    this->RBM.clear();
    
    for(int i = 0; i < nRBM; ++i)
    {
        machine_learning::RBM rbm(
                                  nv_[i],
                                  nh_[i],
                                  // ---
                                  vtype_[i],
                                  htype_[i]
                                  );
        
        this->RBM.push_back(rbm);
    }
}

inline machine_learning::DBN::~DBN() {};
