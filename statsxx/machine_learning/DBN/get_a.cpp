#include "statsxx/machine_learning/DBN.hpp"

// STL
#include <vector> // std::vector<>

// jScience
#include "jScience/linalg.hpp" // Vector<double>


// using namespace machine_learning;


//
// DESC: Gets the visible biases of the DBN.
//
inline std::vector<Vector<double>> machine_learning::DBN::get_a() const
{
    std::vector<Vector<double>> a;

    for(auto i = 0; i < this->RBM.size(); ++i)
    {
        a.push_back(this->RBM[i].get_a());
    }

    return a;
}
