// jScience
#include "jScience/linalg.hpp"                               // Vector<>, Matrix<>, outer_product()

// stats++
#include "statsxx/machine_learning/activation_functions.hpp" // activation_function::Logistic, ::ReLU, ::softplus


inline Vector<double> machine_learning::RBM::h_to_v(
                                                    const Vector<double> &h
                                                    ) const
{
    Vector<double> v;
    
    activation_function::Logistic logistic;
    activation_function::ReLU     ReLU;
    activation_function::softplus softplus;

    v = this->W*h + this->a;

    switch(this->vtype)
    {
        case 0:
            v = logistic.f(v);
            break;
        case 1:
            // v = v;
            break;
        case 2:
            v = ReLU.f(v);
            break;
        case 3:
            v = softplus.f(v);
            break;
        default:
            break;
    }
    
    return v;
}

/*
inline Matrix<double> machine_learning::RBM::v_to_h(
                                                    const Matrix<double> &V
                                                    ) const
{
    Matrix<double> H(V.size(0),nh);
    
    for(auto i = 0; i < V.size(0); ++i)
    {
        Vector<double> h = this->v_to_h(
                                        V.row(i)
                                        );
        
        for(int j = 0; j < nh; ++j)
        {
            H(i,j) = h(j);
        }
    }
    
    return H;
}
*/
