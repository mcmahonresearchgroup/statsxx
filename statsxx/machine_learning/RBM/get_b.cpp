#include "statsxx/machine_learning/RBM.hpp"

// jScience
#include "jScience/linalg.hpp" // Vector<>


// using namespace machine_learning;


inline Vector<double> machine_learning::RBM::get_b() const
{
    return this->b;
}