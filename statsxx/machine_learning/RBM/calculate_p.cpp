#include "statsxx/machine_learning/RBM.hpp"

// jScience
#include "jScience/linalg.hpp" // Vector<>

// stats++
#include "statsxx/machine_learning/activation_functions.hpp" // activation_function::Logistic, ::ReLU, ::softplus


// using namespace machine_learning;


inline Vector<double> machine_learning::RBM::calculate_p(
                                                         const Vector<double> &x,
                                                         const int             type // 0 == binary; 1 == continuous; 2 == ReLU; 3 == softplus
                                                         )
{
    Vector<double> px;
    
    // TODO: I think that the activation member functions could be made static ... then they could be called as commented out below
    activation_function::Logistic logistic;
    activation_function::ReLU     ReLU;
    activation_function::softplus softplus;
    
    switch(type)
    {
        case 0:
            px = logistic.f(x);
            //px = activation_function::Logistic::f(x);
            break;
        case 1:
            px = x;
            break;
        case 2:
            px = ReLU.f(x);
            //px = activation_function::ReLU::f(x);
            break;
        case 3:
            px = softplus.f(x);
            //px = activation_function::softplus::f(x);
            break;
        default:
            break;
    }
    
    return px;
}