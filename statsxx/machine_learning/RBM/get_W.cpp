#include "statsxx/machine_learning/RBM.hpp"

// jScience
#include "jScience/linalg.hpp" // Matrix<>


// using namespace machine_learning;


inline Matrix<double> machine_learning::RBM::get_W() const
{
    return this->W;
}