// jScience
#include "jScience/linalg.hpp"                               // Vector<>, Matrix<>, outer_product()

// stats++
#include "statsxx/machine_learning/activation_functions.hpp" // activation_function::Logistic, ::ReLU, ::softplus


inline Vector<double> machine_learning::RBM::v_to_h(
                                                    const Vector<double> &v
                                                    ) const
{
    activation_function::Logistic logistic;
    activation_function::ReLU     ReLU;
    activation_function::softplus softplus;

    Vector<double> h = transpose(this->W)*v + this->b;

    switch(this->htype)
    {
        case 0:
            h = logistic.f(h);
            break;
        case 1:
            // h = h;
            break;
        case 2:
            h = ReLU.f(h);
            break;
        case 3:
            h = softplus.f(h);
            break;
        default:
            break;
    }
    
    return h;
}


inline Matrix<double> machine_learning::RBM::v_to_h(
                                                    const Matrix<double> &V
                                                    ) const
{
    Matrix<double> H(V.size(0),nh);
    
    for(auto i = 0; i < V.size(0); ++i)
    {
        Vector<double> h = this->v_to_h(
                                        V.row(i)
                                        );
        
        for(int j = 0; j < nh; ++j)
        {
            H(i,j) = h(j);
        }
    }
    
    return H;
}
