#include "statsxx/machine_learning/RBM.hpp"

// jScience
#include "jScience/linalg.hpp" // Matrix<>, Vector<>


// using namespace machine_learning;


inline machine_learning::RBM::RBM() {};

inline machine_learning::RBM::RBM(
                                  const int nv_,    // number of visible units
                                  const int nh_,    // ... hidden
                                  // -----
                                  const int vtype_, // 0 == binary; 1 == continuous; 2 == ReLU; 3 == softplus
                                  const int htype_  // ... same
                                  )
{
    this->nv = nv_;
    this->nh = nh_;
    
    this->vtype = vtype_;
    this->htype = htype_;
    
    this->W = Matrix<double>(this->nv,this->nh);
    this->a = Vector<double>(this->nv);
    this->b = Vector<double>(this->nh);
}

inline machine_learning::RBM::~RBM() {};
