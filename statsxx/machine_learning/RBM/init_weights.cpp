#include "statsxx/machine_learning/RBM.hpp"

// jScience
#include "jrandnum.hpp" // rand_num_normal_Mersenne_twister()


using namespace machine_learning;


inline void RBM::init_weights(
{
    for(auto i = 0; i < this->nv; ++i)
    {
        for(auto j = 0; j < this->nh; ++j)
        {
            this->W(i,j) = 0.1*rand_num_normal_Mersenne_twister(0.,1.);
        }
    }
    
    for(auto i = 0; i < this->nv; ++i)
    {
        this->a(i) = 0.;
    }
    
    for(auto i = 0; i < this->nh; ++i)
    {
        this->b(i) = 0.;
    }
}