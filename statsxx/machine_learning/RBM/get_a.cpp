#include "statsxx/machine_learning/RBM.hpp"

// jScience
#include "jScience/linalg.hpp" // Vector<>


// using namespace machine_learning;


inline Vector<double> machine_learning::RBM::get_a() const
{
    return this->a;
}