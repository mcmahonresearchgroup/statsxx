#ifndef STATSxx_MACHINE_LEARNING_GAUSSIAN_PROCESS_HPP
#define STATSxx_MACHINE_LEARNING_GAUSSIAN_PROCESS_HPP


// STL
#include <utility>   // std::pair<>
#include <vector>    // std::vector<>

// jScience
#include "jScience/linalg/Matrix.hpp" // Matrix<>
#include "jScience/linalg/Vector.hpp" // Vector<>


#include "./Gaussian_process/GaussianProcess.hpp" // GaussianProcess

namespace Gaussian_process
{
    std::pair<std::vector<double>,
              double> optimize_hyperparameters(std::unique_ptr<CovarianceFunction> const &k,
                                               std::vector<double> theta,
                                               const Matrix<double> &X,
                                               const Vector<double> &y,
                                               const Matrix<double> &X_val,
                                               const Vector<double> &y_val,
                                               double sigma_n);
}
#include "./Gaussian_process/src/optimize_hyperparameters.cpp"


#endif
