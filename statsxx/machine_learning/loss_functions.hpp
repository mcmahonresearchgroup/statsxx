#ifndef STATSxx_MACHINE_LEARNING_LOSS_FUNCTIONS_HPP
#define STATSxx_MACHINE_LEARNING_LOSS_FUNCTIONS_HPP


// jScience
#include "jScience/linalg.hpp" // Matrix<>


namespace loss_function
{
    // TODO: NOTE: Perhaps allow probability distributions to be passed (and used) to the calculations of losses over data sets.


    double quadratic(
                     const Vector<double> &f, // predicted value
                     const Vector<double> &o  // observed value
                     );

    double quadratic(
                     const Matrix<double> &f,
                     const Matrix<double> &o
                     );


    double Brier_score(
                       const Matrix<double> &f,
                       const Matrix<double> &o
                       );
}

#include "statsxx/machine_learning/loss_functions/Brier_score.cpp"
#include "statsxx/machine_learning/loss_functions/quadratic.cpp"


#endif
