#ifndef STATSxx_MACHINE_LEARNING_NEURAL_NETWORK_HPP
#define STATSxx_MACHINE_LEARNING_NEURAL_NETWORK_HPP


// stats++
#include "statsxx/dataset.hpp"                    // DataSet
#include "statsxx/machine_learning/NeuralNet.hpp" // NEURAL_NET
// #include "statsxx/machine_learning/neural_network/deep_belief_network/DBN.hpp" // DBN
#include "statsxx/optimization/EA.hpp"            // EAParam


namespace neural_network
{

}

NEURAL_NET NeuralNet_EA_train(
                              NEURAL_NET     mlp,
                              // -----
                              EAParam       &param,
                              // -----
                              const DataSet &ds_tr,
                              const DataSet &ds_val
                              );
#include "statsxx/machine_learning/neural_network/NeuralNet_EA_train.cpp"


#endif
