#ifndef STATSxx_MACHINE_LEARNING_UTILITY_HPP
#define STATSxx_MACHINE_LEARNING_UTILITY_HPP

// STL
#include <utility>  // std::pair<>
#include <vector>   // std::vector<>

// jScience
#include "jScience/linalg.hpp" // Matrix<>, Vector<>


namespace machine_learning
{
    std::pair<
              Matrix<double>, // W
              Vector<double>  // b
              > DBN_to_NN_Wb(
                             const int                          no,
                             const std::vector<Matrix<double>> &W_RBM, // DBN weights
                             const std::vector<Vector<double>> &b_RBM  // DBN hidden biases
                             );
}

#include "statsxx/machine_learning/utility/DBN_to_NN_Wb.cpp"


#endif
