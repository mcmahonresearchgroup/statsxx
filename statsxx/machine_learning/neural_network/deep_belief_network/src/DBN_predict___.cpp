/*
 Copyright 2013--Present JMM_PROGNAME
 
 This file is distributed under the terms of the JMM_PROGNAME License.
 
 You should have received a copy of the JMM_PROGNAME License.
 If not, see <JMM_PROGNAME WEBSITE>.
*/
// CREATED    : 9/17/2015
// LAST UPDATE: 9/30/2015

#include "statsxx/machine_learning/neural_network/deep_belief_network/DBN.hpp"

// STL
#include <vector>   // std::vector<>

// jScience
#include "jScience/linalg/Vector.hpp" // Vector<>


// predict
inline Vector<double> neural_network::DBN::predict(
                                                   const Vector<double> &input
                                                   )
{
    Vector<double> output(input.size());
    
    // PASS THE INPUT THROUGH THE RBMs TO THE LAST UNSUPERVISED LAYER
    
    Vector<double> h = this->RBM_propagate(
                                           neural_network::DBN::PropagationType::deterministic,
                                           this->RBM.size(),
                                           input
                                           );
    
    std::vector<std::vector<double>> MLP_input(1, std::vector<double>(h.size()));
    MLP_input[0] = h.std_vector();
    
    std::vector<double> MLP_output;
    this->MLP.evaluate(MLP_input, MLP_output);
    
    for(auto i = 0; i < output.size(); ++i)
    {
        output(i) = MLP_output[i];
    }
    
    return output;
    
    
/*
    Vector<double> output(input.size());
    
    std::vector<std::vector<double>> MLP_input(1, std::vector<double>(input.size()));
    MLP_input[0] = input.std_vector();
    
    std::vector<double> MLP_output;
    this->MLP.evaluate(MLP_input, MLP_output);
    
    for(auto i = 0; i < output.size(); ++i)
    {
        output(i) = MLP_output[i];
    }
    
    return output;
*/
}
