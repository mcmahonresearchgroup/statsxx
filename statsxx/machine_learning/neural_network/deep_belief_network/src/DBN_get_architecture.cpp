/*
 Copyright 2013--Present JMM_PROGNAME
 
 This file is distributed under the terms of the JMM_PROGNAME License.
 
 You should have received a copy of the JMM_PROGNAME License.
 If not, see <JMM_PROGNAME WEBSITE>.
*/
// CREATED    : 9/30/2015
// LAST UPDATE: 9/30/2015

#include "statsxx/machine_learning/neural_network/deep_belief_network/DBN.hpp"

// STL
#include <vector>   // std::vector<>


// note: this subroutine is provided so that the end-user does not need access to architecture
inline std::vector<int> neural_network::DBN::get_architecture() const
{
    return this->architecture;
}
