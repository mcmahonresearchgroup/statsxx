// TODO: should implement
//
// - identity
// - tanh (regular)


#ifndef STATSxx_MACHINE_LEARNING_ACTIVATION_FUNCTIONS_HPP
#define STATSxx_MACHINE_LEARNING_ACTIVATION_FUNCTIONS_HPP


// stats++
#include "statsxx/machine_learning/activation_functions/ActivationFunction.hpp" // ActivationFunction
#include "statsxx/machine_learning/activation_functions/Logistic.hpp"           // logistic activation function
#include "statsxx/machine_learning/activation_functions/ReLU.hpp"               // rectified linear unit
#include "statsxx/machine_learning/activation_functions/softmax.hpp"            // softmax
#include "statsxx/machine_learning/activation_functions/softplus.hpp"           // softplus (analytical ReLU)
#include "statsxx/machine_learning/activation_functions/tanh_skewed.hpp"        // tanh_skewed


namespace activation_function
{

}


#endif
