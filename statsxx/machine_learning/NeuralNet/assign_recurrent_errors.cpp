#include "statsxx/machine_learning/NeuralNet.hpp"

// STL
#include <iostream>


//========================================================================
//========================================================================
//
// NAME: void NEURAL_NET::assign_recurrent_errors(std::vector<int> &top_neurons)
//
// DESC: Assign recurrent errors into the network, specifying the receiving neurons as top neurons -- those that which are downstream of other top neurons will be removed later, before activation.
//
//========================================================================
//========================================================================
inline void NEURAL_NET::assign_recurrent_errors(std::vector<int> &top_neurons)
{
    for( auto &n : m_neurons )
    {
        for( auto l : m_links_out[n.m_ID] )
        {
            if( m_links[l].tdelay != 0 )
            {
//                n.m_errors.back() += m_neurons[m_links[l].target].get_error(m_links[l].tdelay)*m_links[l].weight;
                top_neurons.push_back(n.m_ID);
            }
        }
    }
}



