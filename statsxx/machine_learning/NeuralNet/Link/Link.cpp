#include "statsxx/machine_learning/NeuralNet/Link.hpp"


inline LINK::LINK()
{
    source = -1;
    target = -1;

    weight = 0.0;

    tdelay = -1;
}


inline LINK::LINK(int s, int t, double w, int td)
{
    source = s;
    target = t;

    weight = w;

    tdelay = td;
}


inline LINK::~LINK() {};
