#ifndef STATSxx_MACHINE_LEARNING_NEURALNET_UTILITY_HPP
#define STATSxx_MACHINE_LEARNING_NEURALNET_UTILITY_HPP


// STL
#include <utility> // std::pair<>
#include <vector>  // std::vector<>


std::pair<
          std::vector<double>,
          std::vector<double>::size_type
          > fit_validation_error(
                                 const std::vector<double> E,
                                 const std::vector<double> stddevE
                                 );

#include "statsxx/machine_learning/NeuralNet/utility/fit_validation_error.cpp"


#endif
