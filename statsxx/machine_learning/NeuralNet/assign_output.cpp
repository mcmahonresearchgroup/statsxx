#include "statsxx/machine_learning/NeuralNet.hpp"

// STL
#include <vector> // std::vector<>


//
// DESC: Assigns the output at the most recent (current) time to vector output.
//
inline void NEURAL_NET::assign_output(std::vector<double> &output)
{
    output.clear();

    for( auto n : m_out_neurons )
    {
        output.push_back( m_neurons[n].m_output.front() );
    }
}
