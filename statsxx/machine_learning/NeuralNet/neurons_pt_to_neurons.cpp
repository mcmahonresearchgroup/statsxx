#include "statsxx/machine_learning/NeuralNet.hpp"

// STL
#include <iostream>

// jSCIENCE
#include "jutility.hpp"


//========================================================================
//========================================================================
//
// NAME: bool NEURAL_NET::neurons_pt_to_neurons(const std::vector<int> &neurons, bool forward)
//
// DESC: Checks whether a vector of neurons point upstream (forward) or downstream (backward) to other neurons (non-recurrent), and not just OUTPUT, INPUT, or BIAS neurons).
//
//========================================================================
//========================================================================
inline bool NEURAL_NET::neurons_pt_to_neurons(const std::vector<int> &neurons, bool _forward)
{
    if( _forward )
    {
        for( auto n : neurons )
        {
            if( m_lkupTbl_neurPtToNeur_for[n] )
            {
                return true;
            }
        }
    }
    else
    {
        for( auto n : neurons )
        {
            if( m_lkupTbl_neurPtToNeur_back[n] )
            {
                return true;
            }
        }
    }


    return false;
}

