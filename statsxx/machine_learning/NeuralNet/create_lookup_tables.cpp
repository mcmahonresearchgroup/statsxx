#include "statsxx/machine_learning/NeuralNet.hpp"

// STL
#include <iostream>


//========================================================================
//========================================================================
//
// NAME: void NEURAL_NET::create_lookup_tables()
//
// DESC: Creates lookup tables so that certain computationally demanding checks do not have to be repeatedly made.
//
//========================================================================
//========================================================================
inline void NEURAL_NET::create_lookup_tables()
{
    // CHECK WHICH NEURONS POINT TO OTHER NEURONS (AND NOT JUST OUTPUT, INPUT, OR BIAS NEURONS) -- FOR neurons_pt_to_neurons() ...
    m_lkupTbl_neurPtToNeur_for.assign(  m_neurons.size(), false );
    m_lkupTbl_neurPtToNeur_back.assign( m_neurons.size(), false );

    for( auto &n : m_neurons )
    {
        for( auto l : m_links_out[n.m_ID] )
        {
            if( (m_links[l].tdelay == 0) && (m_neurons[m_links[l].target].type == NEURON::TYPE::HIDDEN) )
            {
                m_lkupTbl_neurPtToNeur_for[n.m_ID]  = true;
                break;
            }
        }

        for( auto l : m_links_in[n.m_ID] )
        {
            if( (m_links[l].tdelay == 0) && (m_neurons[m_links[l].source].type == NEURON::TYPE::HIDDEN) )
            {
                m_lkupTbl_neurPtToNeur_back[n.m_ID] = true;
                break;
            }
        }
    }

    // CREATE CONNECTION MATRIX
    m_lkupTbl_connections.assign( m_neurons.size(), std::vector<int>( m_neurons.size(), 0 ) );

    for( decltype(m_neurons.size()) j = 0; j < (m_neurons.size()-1); ++j )
    {
        for( decltype(m_neurons.size()) k = (j+1); k < m_neurons.size(); ++k )
        {
            if( check_for_connection(m_neurons[j], m_neurons[k]) )
            {
                m_lkupTbl_connections[j][k] = 1;
                m_lkupTbl_connections[k][j] = -1;
            }
            else if( check_for_connection(m_neurons[k], m_neurons[j]) )
            {
                m_lkupTbl_connections[k][j] = 1;
                m_lkupTbl_connections[j][k] = -1;
            }
        }
    }
}

