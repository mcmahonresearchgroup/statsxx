#ifndef STATSxx_MACHINE_LEARNING_NEURALNET_LINK_HPP
#define STATSxx_MACHINE_LEARNING_NEURALNET_LINK_HPP


// Boost
#include <boost/serialization/serialization.hpp> // boost::serialization::

// stats++
#include "statsxx/machine_learning/NeuralNet/Neuron.hpp"


//=========================================================
// LINK
//=========================================================
class LINK
{

public:

    int     source;
    int     target;

    double  weight;
    int     tdelay;

    LINK();
    LINK(int s, int t, double w, int td);
    ~LINK();

private:

    // (Boost) SERIALIZATION
    friend class boost::serialization::access;

    template<class Archive>
    void serialize(
                   Archive            &ar,
                   const unsigned int  version
                   )
    {
        ar & this->source;
        ar & this->target;

        ar & this->weight;
        ar & this->tdelay;
    }

};

#include "statsxx/machine_learning/NeuralNet/Link/Link.cpp"


#endif
