#include "statsxx/machine_learning/NeuralNet.hpp"

// STL
#include <vector>

// stats++
#include "statsxx/machine_learning/activation_functions.hpp" // activation_function::softmax


//
// DESC: Propagates
//
inline void NEURAL_NET::forward_prop(
                                     std::vector<double> &input
                                     )
{
    this->load_input(input);

    for( auto &l : layer_neuron_idx )
    {
        this->activate_neurons(l, true);
    }

    if( this->m_isClassif && (this->m_nout > 1) )
    {
        activation_function::softmax softmax;

        std::vector<double> softmax_x;
        for( const auto &n : this->m_out_neurons )
        {
            softmax_x.push_back( this->m_neurons[n].get_output(0) );
        }
        softmax_x = softmax.f(softmax_x);

        for( decltype(this->m_out_neurons.size()) i = 0; i < this->m_out_neurons.size(); ++i )
        {
            m_neurons[m_out_neurons[i]].override_output( 0, softmax_x[i] );
            m_neurons[m_out_neurons[i]].override_activation(true);
        }
    }
}

