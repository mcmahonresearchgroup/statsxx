#include "statsxx/machine_learning/NeuralNet.hpp"

// STL
#include <vector>


//
// DESC: Loads input into the network.
//
inline void NEURAL_NET::load_input(
                                   std::vector<double> input
                                   )
{
    // << JMM >> :: THIS RELIES ON THE INPUT NEURONS TO BE PLACED IN 0, ..., n-1 SO WE CAN USE n TO ACCESS input
    for( auto n : m_inp_neurons )
    {
        m_neurons[n].m_output.front() = input[n];
    }

    // << JMM >> :: there MAY be a more efficient way to call this, so that on every load_input() call we don't have to reset the bias neurons to 1.0 (since they ALWAYS) stay that way -- but then it becomes less easy to clear the output from the network
    for( auto n : m_bias_neurons )
    {
        m_neurons[n].m_output.front() = 1.0;
    }
}
