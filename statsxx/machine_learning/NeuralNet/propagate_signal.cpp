#include "statsxx/machine_learning/NeuralNet.hpp"

// STL
#include <iostream>


//========================================================================
//========================================================================
//
// NAME: void NEURAL_NET::propagate_signal(NEURON &n, std::vector<int> &hit_list, bool forward)
//
// DESC: Propagates a (non-recurrent) signal (e.g., input or error) either forward or backward as far as it will travel, storing neurons hit in hit_list. Note that updates to m_output or m_errors do not actually occur.
//
//========================================================================
//========================================================================
inline void NEURAL_NET::propagate_signal(NEURON &n, std::vector<int> &hit_list, bool _forward)
{
    if( _forward )
    {
        for( auto l : m_links_out[n.m_ID] )
        {
            if( m_links[l].tdelay == 0 )
            {
                // ERRORS CONTINUE TO PROPAGATE THROUGH ACTIVATED NEURONS ...
                if( m_neurons[m_links[l].target].is_activated() )
                {
                    std::cout << "here in propagating signal, not sure if we would ever hit here!" << std::endl;
                    int ui; std::cin >> ui;
                    propagate_signal(m_neurons[m_links[l].target], hit_list, _forward);
                }
                // ... ELSE ADD NEURON TO HIT LIST ...
                else
                {
                    hit_list.push_back(m_links[l].target);
                }
            }
        }
    }
    else
    {
        for( auto l : m_links_in[n.m_ID] )
        {
            if( m_links[l].tdelay == 0 )
            {
                if( m_neurons[m_links[l].source].is_activated() )
                {
                    std::cout << "here in propagating signal, not sure if we would ever hit here!" << std::endl;
                    int ui; std::cin >> ui;
                    propagate_signal(m_neurons[m_links[l].source], hit_list, _forward);
                }
                else
                {
                    hit_list.push_back(m_links[l].source);
                }
            }
        }
    }
}

