#include "statsxx/machine_learning/NeuralNet.hpp"

// STL
#include <iostream>

// jSCIENCE
#include "jutility.hpp"


//========================================================================
//========================================================================
//
// NAME: bool NEURAL_NET::check_for_connection(NEURON &n1, NEURON &n2)
//
// DESC: Check for a (non-recurrent) connection n1 --> n2.
//
//========================================================================
//========================================================================
inline bool NEURAL_NET::check_for_connection(NEURON &n1, NEURON &n2)
{
    // CHECK FOR ONLY RECURRENT CONNECTIONS UP FRONT -- THIS PREVENTS INFINITE LOOPS WITH RECURSIVE check_for_connection() CALLS IF A NEURON HAS ONLY RECURRENT CONNECTIONS (E.G., OUTPUT NEURONS)
    bool only_recurrent = true;

    for( auto l : m_links_out[n1.m_ID] )
    {
        if( m_links[l].tdelay == 0 )
        {
            only_recurrent = false;
        }
    }

    if( only_recurrent )
    {
        return false;
    }


//    std::cout << "n1.m_ID: " << n1.m_ID << std::endl;

    for( auto l : m_links_out[n1.m_ID] )
    {
//        std::cout << "m_links[l].target: " << m_links[l].target << std::endl;

        if( m_links[l].tdelay == 0 ) // don't traverse a recurssive connection
        {
            if( (m_links[l].target == n2.m_ID) || check_for_connection(m_neurons[m_links[l].target], n2) )
            {
                return true;
            }
        }
    }


    return false;
}
