#include "statsxx/machine_learning/NeuralNet.hpp"

// STL
#include <iostream>


//========================================================================
//========================================================================
//
// NAME: void NEURAL_NET::activate_neurons(std::vector<int> &neurons, bool forward)
//
// DESC: Propagates a (non-recurrent) signal (e.g., input or error) either forward or backward as far as it will travel, storing neurons hit in hit_list. Note that
//
//========================================================================
//========================================================================
inline void NEURAL_NET::activate_neurons(std::vector<int> &neurons, bool _forward)
{
    if( _forward )
    {
        for( auto n : neurons )
        {
            for( auto l : m_links_in[m_neurons[n].m_ID] )
            {
                m_neurons[n].m_output.front() += m_neurons[m_links[l].source].get_output(m_links[l].tdelay)*m_links[l].weight;
            }

            m_neurons[n].activate();
        }
    }
    else
    {
        for( auto n : neurons )
        {
            for( auto l : m_links_out[m_neurons[n].m_ID] )
            {
                m_neurons[n].m_errors.back() += m_neurons[m_links[l].target].get_error(m_links[l].tdelay)*m_links[l].weight;
            }

            m_neurons[n].activate_error();
        }
    }
}


