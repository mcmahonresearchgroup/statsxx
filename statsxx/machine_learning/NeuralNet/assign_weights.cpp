#include "statsxx/machine_learning/NeuralNet.hpp"

// STL
#include <vector> // std::vector


//
// DESC: Assigns the weights w to each link.
//
// NOTE: This is useful, for example, for training methods to quickly change all weights.
//
inline void NEURAL_NET::assign_weights(const std::vector<double> &w)
{
    for( decltype(m_links.size()) i = 0; i < m_links.size(); ++i )
    {
        m_links[i].weight = w[i];
    }
}
