#include "statsxx/machine_learning/NeuralNet.hpp"


inline NEURAL_NET::NEURAL_NET()
{
    init();
}

inline NEURAL_NET::~NEURAL_NET() {}


//
// DESC: (Re-)Initialized all neural network memory.
//
inline void NEURAL_NET::init()
{
    m_isClassif      = false;

    m_ninp           = 0;
    m_nout           = 0;

    m_neurons.clear();
    m_inp_neurons.clear();
    m_bias_neurons.clear();
    m_out_neurons.clear();

    m_links.clear();
    m_links_in.clear();
    m_links_out.clear();
}


//========================================================================
//========================================================================
//
// NAME: void NEURAL_NET::clear_memory()
//
// DESC: Clears each neuron's memory (for recurrent links).
//
//========================================================================
//========================================================================
inline void NEURAL_NET::clear_memory()
{
    for( auto &n : m_neurons )
    {
        n.clear_memory();
    }
}


//========================================================================
//========================================================================
//
// NAME: void NEURAL_NET::clear_errors()
//
// DESC: Clears each neuron's error history.
//
//========================================================================
//========================================================================
inline void NEURAL_NET::clear_errors()
{
    for( auto &n : m_neurons )
    {
        n.clear_errors();
    }
}


//========================================================================
//========================================================================
//
// NAME: void NEURAL_NET::deactivate_network()
//
// DESC: De-activates all neurons in the network. Note that memory is preserved, and this is primarily used to de-activate the neurons for succesive temporal inputs.
//
//========================================================================
//========================================================================
inline void NEURAL_NET::deactivate_network()
{
    for( auto &n : m_neurons )
    {
        n.deactivate();
    }
}
