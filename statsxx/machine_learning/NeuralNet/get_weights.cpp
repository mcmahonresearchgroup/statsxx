#include "statsxx/machine_learning/NeuralNet.hpp"

// STL
#include <vector> // std::vector


//
// DESC: Gets the weights w for each link.
//
inline std::vector<double> NEURAL_NET::get_weights()
{
    std::vector<double> _w;

    for( auto i = 0; i < m_links.size(); ++i )
    {
        _w.push_back(m_links[i].weight);
    }

    return _w;
}
