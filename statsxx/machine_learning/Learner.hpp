#ifndef STATSxx_MACHINE_LEARNING_Learner_HPP
#define STATSxx_MACHINE_LEARNING_Learner_HPP

// STL
#include <vector>                                // std::vector<>

// Boost
#include <boost/serialization/serialization.hpp> // boost::serialization::
#include <boost/serialization/vector.hpp>        // serialize std::vector<>

// jScience
#include "jScience/linalg.hpp"                   // Matrix<>

// stats++
#include "statsxx/machine_learning/Learner.hpp"  // Learner


//=========================================================
// LEARNER
//=========================================================
//
// NOTE: The purpose of a Learner object is to integrate into the Ensemble class.
//
// NOTE: ... It guarantees the existence of certain functions (e.g., f_train).
//
class Learner
{

public:

    Learner();

    ~Learner();


    std::function<void(const Matrix<double> &, const Matrix<double> &)> f_train;

    std::function<std::vector<double>(const std::vector<double> &)> f_evaluate;


private:

    // (Boost) SERIALIZATION
    friend class boost::serialization::access;

    template<class Archive>
    void serialize(
                   Archive            &ar,
                   const unsigned int  version
                   )
    {
//        ar & *this;
    }

};

#include "statsxx/machine_learning/Learner/Learner.cpp"


#endif
