#ifndef STATSxx_MACHINE_LEARNING_ACTIVATION_FUNCTIONS_TANH_SKEWED_HPP
#define STATSxx_MACHINE_LEARNING_ACTIVATION_FUNCTIONS_TANH_SKEWED_HPP


// stats++
#include "statsxx/machine_learning/activation_functions/ActivationFunction.hpp" // ActivationFunction


namespace activation_function
{
    //=========================================================
    // HYPERBOLIC TANGENT FUNCTION (SKEWED)
    //
    // NOTE: See: ``Efficient BackProp'' by LeCun et al. (1998)
    //=========================================================
    class tanh_skewed : public ActivationFunction
    {

    public:

        using ActivationFunction::f;

        tanh_skewed();
        ~tanh_skewed();

        double f(const double x);
        double df(const double x);
        double inv(const double fx);

    };
}

#include "statsxx/machine_learning/activation_functions/src/tanh_skewed.cpp"


#endif
