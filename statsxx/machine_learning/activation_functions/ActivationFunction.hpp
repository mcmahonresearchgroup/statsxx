#ifndef STATSxx_MACHINE_LEARNING_ACTIVATION_FUNCTIONS_ACTIVATIONFUNCTION_HPP
#define STATSxx_MACHINE_LEARNING_ACTIVATION_FUNCTIONS_ACTIVATIONFUNCTION_HPP


// jScience
#include "jScience/linalg/Vector.hpp" // Vector<>


// TODO: The following member functions of ActivationFunction should be made static, if possible. Then the member functions could be called without creating an instance of the object (e.g.,  activation_function::Logistic::f(x))


namespace activation_function
{
    //=========================================================
    // ACTIVATION FUNCTION
    //=========================================================
    class ActivationFunction
    {
        
    public:
        
        ActivationFunction();
        virtual ~ActivationFunction() = 0;
        
        virtual double f(const double x) = 0;
        virtual double df(const double x) = 0;
        virtual double inv(const double x) = 0;
        
        Vector<double> f(const Vector<double> &x);
        
    };
}

#include "statsxx/machine_learning/activation_functions/src/ActivationFunction.cpp"


#endif
