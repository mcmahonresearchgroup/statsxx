#ifndef STATSxx_MACHINE_LEARNING_ACTIVATION_FUNCTIONS_SOFTMAX_HPP
#define STATSxx_MACHINE_LEARNING_ACTIVATION_FUNCTIONS_SOFTMAX_HPP


// STL
#include <vector> // std::vector<>

// stats++
#include "statsxx/machine_learning/activation_functions/ActivationFunction.hpp" // ActivationFunction


namespace activation_function
{
    //=========================================================
    // SOFTMAX
    //=========================================================
    class softmax : public ActivationFunction
    {

    public:

        using ActivationFunction::f;

        softmax();
        ~softmax();

        double f(const double x);
        double df(const double x);
        double inv(const double fx);

        std::vector<double> f(std::vector<double> x);
        double df(const std::vector<double> &x, int k, int j);

    };
}

#include "statsxx/machine_learning/activation_functions/src/softmax.cpp"


#endif
