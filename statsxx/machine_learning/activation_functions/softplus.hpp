#ifndef STATSxx_MACHINE_LEARNING_ACTIVATION_FUNCTIONS_SOFTPLUS_HPP
#define STATSxx_MACHINE_LEARNING_ACTIVATION_FUNCTIONS_SOFTPLUS_HPP


// jScience
#include "jScience/linalg/Vector.hpp" // Vector<>

// stats++
#include "statsxx/machine_learning/activation_functions/ActivationFunction.hpp" // ActivationFunction


namespace activation_function
{
    //=========================================================
    // softplus (analytical approximation to ReLU)
    //
    // NOTES:
    //     - see: https://en.wikipedia.org/wiki/Rectifier_(neural_networks)
    //=========================================================
    class softplus : public ActivationFunction
    {
        
    public:
        
        using ActivationFunction::f;
        
        softplus();
        ~softplus();
        
        double f(const double x);
        double df(const double x);
        double inv(const double fx);
        
    };
}

#include "statsxx/machine_learning/activation_functions/src/softplus.cpp"


#endif
