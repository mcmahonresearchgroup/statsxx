#ifndef STATSxx_MACHINE_LEARNING_ACTIVATION_FUNCTIONS_LOGISTIC_HPP
#define STATSxx_MACHINE_LEARNING_ACTIVATION_FUNCTIONS_LOGISTIC_HPP


// jScience
#include "jScience/linalg/Vector.hpp" // Vector<>

// stats++
#include "statsxx/machine_learning/activation_functions/ActivationFunction.hpp" // ActivationFunction


namespace activation_function
{
    //=========================================================
    // LOGISTIC FUNCTION
    //
    // Notes:
    //     - This function has a response of 1.
    //=========================================================
    class Logistic : public ActivationFunction
    {

    public:

        using ActivationFunction::f;

        Logistic();
        ~Logistic();

        double f(const double x);
        double df(const double x);
        double inv(const double fx);

    };
}

#include "statsxx/machine_learning/activation_functions/src/Logistic.cpp"


#endif
