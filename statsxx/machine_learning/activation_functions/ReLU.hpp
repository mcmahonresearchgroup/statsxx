#ifndef STATSxx_MACHINE_LEARNING_ACTIVATION_FUNCTIONS_ReLU_HPP
#define STATSxx_MACHINE_LEARNING_ACTIVATION_FUNCTIONS_ReLU_HPP


// jScience
#include "jScience/linalg/Vector.hpp" // Vector<>

// stats++
#include "statsxx/machine_learning/activation_functions/ActivationFunction.hpp" // ActivationFunction


namespace activation_function
{
    //=========================================================
    // (Re)ctified (L)inear (U)nit
    //
    // NOTES:
    //     - See: https://en.wikipedia.org/wiki/Rectifier_(neural_networks)
    //=========================================================
    class ReLU : public ActivationFunction
    {
        
    public:
        
        using ActivationFunction::f;
        
        ReLU();
        ~ReLU();
        
        double f(const double x);
        double df(const double x);
        double inv(const double x);
        
    };
}

#include "statsxx/machine_learning/activation_functions/src/ReLU.cpp"


#endif
