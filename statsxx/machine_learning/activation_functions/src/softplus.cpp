#include "statsxx/machine_learning/activation_functions/softplus.hpp"

// STL
#include <cmath> // std::exp(), std::log()


inline activation_function::softplus::softplus() {};

inline activation_function::softplus::~softplus() {};


//     f(x) = ln(1 + e^x)
inline double activation_function::softplus::f(const double x)
{
    return std::log(1. + std::exp(x));
}

//     f'(x) = 1/(1 + e^-x)
inline double activation_function::softplus::df(const double x)
{
    return ( 1./(1. + std::exp(-x)) );
}

//     x = ln(e^f(x) - 1)
inline double activation_function::softplus::inv(const double fx)
{
    return std::log(std::exp(fx) - 1.);
}