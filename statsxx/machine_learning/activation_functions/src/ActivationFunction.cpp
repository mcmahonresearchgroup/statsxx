/*
 Copyright 2013--Present JMM_PROGNAME
 
 This file is distributed under the terms of the JMM_PROGNAME License.
 
 You should have received a copy of the JMM_PROGNAME License.
 If not, see <JMM_PROGNAME WEBSITE>.
*/
// CREATED    : 9/11/2015
// LAST UPDATE: 9/30/2015

#include "statsxx/machine_learning/activation_functions/ActivationFunction.hpp"


inline activation_function::ActivationFunction::ActivationFunction() {};

inline activation_function::ActivationFunction::~ActivationFunction() {};

inline Vector<double> activation_function::ActivationFunction::f(const Vector<double> &x)
{
    Vector<double> fx(x.size());
    
    for(auto i = 0; i < x.size(); ++i)
    {
        fx(i) = this->f(x(i));
    }
    
    return fx;
}
