#include "statsxx/machine_learning/Ensemble.hpp"


// STL
#include <vector>              // std::vector<>

// jScience
#include "jScience/linalg.hpp" // Matrix<>


//
// DESC: Combine the output each learner.
//
// NOTE: This is a utility subroutine, for where the output from each learner over a training set is used repeatedly (e.g., to optimize weights).
//
template<typename T>
inline Matrix<double> Ensemble<T>::combine_output(
                                                  const std::vector<Matrix<double>> &X_i
                                                  )
{
    // NOTE: Element 0 of X_i is used to determine the size of X.

    Matrix<double> X(X_i[0].size(0),X_i[0].size(1), 0.);

    for( auto i = 0; i < X_i[0].size(0); ++i )
    {
        for( auto j = 0; j < X_i[0].size(1); ++j )
        {
            for( auto k = 0; k < X_i.size(); ++k )
            {
                X(i,j) += ( this->w[k]*X_i[k](i,j) );
            }
        }
    }

    return X;
}
