#include "statsxx/machine_learning/Ensemble.hpp"

// STL
#include <fstream> // std::ofstream
#include <string>  // std::string


//
// DESC: Output Ensemble weights.
//
template<typename T>
inline void Ensemble<T>::output_weights(
                                        const std::string prefix
                                        )
{
    std::ofstream ofs( (prefix + ".w.dat") );

    for( auto i = 0; i < this->w.size(); ++i )
    {
        ofs << this->w[i] << '\n';
    }

    ofs.close();
}
