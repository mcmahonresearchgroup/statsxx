#include "statsxx/machine_learning/Ensemble.hpp"


template<typename T>
inline Ensemble<T>::Ensemble()
{
    this->_evaluate_Boost = false;
}

template<typename T>
inline Ensemble<T>::Ensemble(
                             const bool _is_classif,
                             // -----
                             const int  _ni,
                             const int  _no
                             )
{
    this->is_classif      = _is_classif;

    this->ni              = _ni;
    this->no              = _no;

    this->_evaluate_Boost = false;
}


template<typename T>
inline Ensemble<T>::~Ensemble() {};
