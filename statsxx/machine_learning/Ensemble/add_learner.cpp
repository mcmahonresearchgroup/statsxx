#include "statsxx/machine_learning/Ensemble.hpp"


//
// DESC: Add a learner to the ensemble.
//
// NOTE: Calling this subroutine also (re-)initializes the weights.
//
// TODO: NOTE: ... This may change, in the future.
//
template<typename T>
inline void Ensemble<T>::add_learner(
                                     const T &_learner
                                     )
{
    this->learners.push_back(_learner);

    this->init_weights();
}
