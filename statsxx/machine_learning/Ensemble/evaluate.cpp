#include "statsxx/machine_learning/Ensemble.hpp"


// STL
#include <vector> // std::vector<>


//
// DESC: Use the ensemble to evaluate a given input.
//
// NOTE: The ensemble evaluates an input as a weighted average:
//
//     out = sum_i w_i out_i
//
template<typename T>
inline std::vector<double> Ensemble<T>::evaluate(
                                                 const std::vector<double> &input
                                                 )
{
    // TODO: Should probably make the idea of evaluation flag more standard (i.e., for all types of evaluation).
    // TODO: NOTE: ... Things would then become more seamless.
    if( this->_evaluate_Boost )
    {
        return this->evaluate_Boost(
                                    input
                                    );
    }

    // TODO: ... The following would then go to evaluate_wlinear().
    std::vector<double> output(no, 0.);

    for( auto i = 0; i < this->learners.size(); ++i )
    {
        output += this->w[i]*this->learners[i].f_evaluate(input);
    }

    return output;
}
