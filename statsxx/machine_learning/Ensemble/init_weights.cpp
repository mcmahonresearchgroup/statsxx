#include "statsxx/machine_learning/Ensemble.hpp"


// STL
#include <vector> // std::vector<>


//
// DESC: Initialize (uniformly) the weights of the ensemble.
//
template<typename T>
inline void Ensemble<T>::init_weights()
{
    this->w.clear();

    this->w = std::vector<double>(this->learners.size(), (1./this->learners.size()));
}
