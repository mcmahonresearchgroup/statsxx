// jScience
#include "jScience/linalg.hpp" // Matrix<>

// stats++
#include "statsxx/machine_learning/loss_functions.hpp" // loss_function::quadratic()


//
// DESC: Calculate the Brier score.
//
// NOTE: See:
//
//     http://en.wikipedia.org/wiki/Brier_score
//
// NOTE: The following uses the original definition by Brier.
//
inline double loss_function::Brier_score(
                                         const Matrix<double> &f,
                                         const Matrix<double> &o
                                         )
{
    // NOTE: The (original-definition) Brier score is the same as the quadratic error.

    return loss_function::quadratic(
                                    f,
                                    o
                                    );
}
