// jScience
#include "jScience/linalg.hpp" // Vector<>, Matrix<>, dot_product()

// stats++
#include "statsxx/machine_learning/loss_functions.hpp" // loss_function::quadratic()


//
// DESC: Calculate the quadratic error of a point.
//
inline double loss_function::quadratic(
                                       const Vector<double> &f, // predicted value
                                       const Vector<double> &o  // observed value
                                       )
{
    Vector<double> o_f = o - f;

    return dot_product(o_f, o_f);
}



//
// DESC: Calculate the quadratic error, over a data set.
//
// NOTE: This subroutine assumes that the probability of each value is the same.
//
// NOTE: In this case (and for one output), this becomes the mean squared error.
//
inline double loss_function::quadratic(
                                       const Matrix<double> &f,
                                       const Matrix<double> &o
                                       )
{
    double quadratic_error = 0.;

    for( auto t = 0; t < o.size(0); ++t )
    {
        quadratic_error += loss_function::quadratic(
                                                    f.row(t),
                                                    o.row(t)
                                                    );
    }

    quadratic_error /= o.size(0);

    return quadratic_error;
}
