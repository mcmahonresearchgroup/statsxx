/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of FDTD++.
 
 FDTD++ is proprietary software: you can use it and/or modify it
 under the terms of the Algorithms in Motion License as published by
 Algorithms in Motion LLC, either version 1 of the License, or (at your
 option) any later version.
 
 FDTD++ is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 Algorithms in Motion License for more details.
 
 You should have received a copy of the Algorithms in Motion License
 along with FDTD++. If not, see <http://www.aimotionllc.com/licenses/>.
*/
// CREATED    : 9/14/2015
// LAST UPDATE: 10/12/2015

#include "statsxx/machine_learning/restricted_Boltzmann_machine/RBM.hpp"


// note: this subroutine (and get_nhid()) are provided so that the end-user does not need access to nvis (and nhid)
inline int restricted_Boltzmann_machine::RBM::get_nvis() const
{
    return this->nvis;
}

