/*
 Copyright 2013--Present JMM_PROGNAME
 
 This file is distributed under the terms of the JMM_PROGNAME License.
 
 You should have received a copy of the JMM_PROGNAME License.
 If not, see <JMM_PROGNAME WEBSITE>.
*/
// CREATED    : 7/26/2015
// LAST UPDATE: 9/13/2015

#ifndef STATSxx_MACHINE_LEARNING_RESTRICTED_BOLZTMANN_MACHINE_HPP
#define STATSxx_MACHINE_LEARNING_RESTRICTED_BOLZTMANN_MACHINE_HPP


#include "statsxx/machine_learning/restricted_Boltzmann_machine/RBM.hpp" // RBM


namespace restricted_Boltzmann_machine
{

}


#endif
