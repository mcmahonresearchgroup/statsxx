// STL
#include <vector> // std::vector<>


template<typename T>
inline std::vector<std::vector<T>> statsxx::utility::XY(
                                                        const std::vector<T>& x,
                                                        const std::vector<T>& y
                                                        )
{
    std::vector<std::vector<T>> xy;

    for (auto& _x : x)
    {
         std::vector<T> _xy;

         for (auto& _y : y)
         {
             _xy.push_back(_x*_y);
         }

         xy.push_back(_xy);
    }

    return xy;
}
