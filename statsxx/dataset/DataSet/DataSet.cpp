#include "statsxx/dataset/DataSet.hpp"

// stats++
#include "statsxx/dataset/DataPoint.hpp" // DataPoint


inline DataSet::DataSet() {};

inline DataSet::DataSet(int n, int nt, int ni, int no)
{
    pt.resize(n, DataPoint(nt, ni, no));
}

inline DataSet::~DataSet() {};

