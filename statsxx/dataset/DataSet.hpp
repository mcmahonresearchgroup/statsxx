#ifndef STATSxx_DATASET_DATASET_HPP
#define STATSxx_DATASET_DATASET_HPP


// STL
#include <string> // std::string
#include <vector> // std::vector<>

// This
#include "statsxx/dataset/DataPoint.hpp" // DataPoint


//=========================================================
// DataSet
//=========================================================
class DataSet
{

private:

public:

    std::string            id;

    std::vector<DataPoint> pt;


    DataSet();
    DataSet(int n, int nt, int ni, int no);
    ~DataSet();

};

#include "statsxx/dataset/DataSet/DataSet.cpp"


#endif
