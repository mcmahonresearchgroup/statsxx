#include "statsxx/dataset/DataPoint.hpp"

// STL
#include <vector> // std::vector


inline DataPoint::DataPoint() {};

inline DataPoint::DataPoint(int nt, int ni, int no)
{
    in.resize( nt, std::vector<double>(ni, 0.0) );
    out.resize( no, 0.0 );
}

inline DataPoint::~DataPoint() {};


