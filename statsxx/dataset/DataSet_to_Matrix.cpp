// STL
#include <utility>                     // std::pair<>, std::make_pair()

// jScience
#include "jScience/linalg/Matrix.hpp"  // Matrix<double>

// stats++
#include "statsxx/dataset/DataSet.hpp" // DataSet


inline std::pair<
                 Matrix<double>,
                 Matrix<double>
                 > DataSet_to_Matrix(
                                     const DataSet &data_set
                                     )
{
    Matrix<double> X(data_set.pt.size(), data_set.pt[0].in[0].size());
    Matrix<double> X_out(data_set.pt.size(), data_set.pt[0].out.size());

    for(auto i = 0; i < data_set.pt.size(); ++i)
    {
        for(auto j = 0; j < data_set.pt[i].in[0].size(); ++j)
        {
            X(i,j) = data_set.pt[i].in[0][j];
        }

        for(auto j = 0; j < data_set.pt[i].out.size(); ++j)
        {
            X_out(i,j) = data_set.pt[i].out[j];
        }
    }

    return std::make_pair(
                          X,
                          X_out
                          );
}
