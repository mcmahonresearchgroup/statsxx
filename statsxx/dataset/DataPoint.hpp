#ifndef STATSxx_DATASET_DATAPOINT_HPP
#define STATSxx_DATASET_DATAPOINT_HPP


// STL
#include <string> // std::string
#include <vector> // std::vector


//=========================================================
// DataPoint
//=========================================================
// TODO: the in should probably be made into a Matrix class (fast, 1D)p
class DataPoint
{

public:

    std::string                      id;

    std::vector<std::vector<double>> in;
    std::vector<double>              out;


    DataPoint();
    DataPoint(int nt, int ni, int no);
    ~DataPoint();

};

#include "statsxx/dataset/DataPoint/DataPoint.cpp"


#endif
