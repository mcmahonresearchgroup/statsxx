// jScience
#include "jScience/linalg/Matrix.hpp"  // Matrix<double>

// stats++
#include "statsxx/dataset/DataSet.hpp" // DataSet


inline DataSet Matrix_to_DataSet(
                                 const Matrix<double> &X,          // data
                                 const Matrix<double> &X_out       // data (output)
                                 )
{
    DataSet data_set(X.size(0), 1, X.size(1), X_out.size(1));

    for(auto i = 0; i < X.size(0); ++i)
    {
        for(auto j = 0; j < X.size(1); ++j)
        {
            data_set.pt[i].in[0][j] = X(i,j);
        }

        for(auto j = 0; j < X_out.size(1); ++j)
        {
            data_set.pt[i].out[j] = X_out(i,j);
        }
    }

    return data_set;
}
