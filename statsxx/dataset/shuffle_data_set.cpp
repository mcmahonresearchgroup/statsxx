// jScience
#include "jScience/stats/data.hpp" // shuffle_data()

// stats++
#include "statsxx/dataset/DataSet.hpp" // DataSet


//
// DESC: Shuffle a DataSet object.
//
// INPUT:
//     DataSet data_set      : Data set to be shuffled
//
// OUTPUT:
//     DataSet data_set      : Shuffled data set.
//
inline void shuffle_data_set( DataSet &data_set )
{
    shuffle_data(data_set.pt);
}
