/*
 Copyright 2013--Present JMM_PROGNAME
 
 This file is distributed under the terms of the JMM_PROGNAME License.
 
 You should have received a copy of the JMM_PROGNAME License.
 If not, see <JMM_PROGNAME WEBSITE>.
*/
// CREATED    : 10/17/2015
// LAST UPDATE: 10/17/2015


// STL
#include <vector>     // std::vector<>


//========================================================================
//========================================================================
//
// NAME: double statistics::NMSE(
//                               const std::vector<double> &x,
//                               const std::vector<double> &y
//                               )
//
// DESC: Computes the normalized mean squared error.
//
// NOTES:
//
//     - I believe that this is from:
//
//     S. R. Hanna, D. W. Heinold, "Development and Application of a Simple Method for Evaluating Air Quality Models," API Publ. No. 4409, American Petroleum Institute, Washington, DC, 1985. 22. J. C.
//
//========================================================================
//========================================================================
inline double statistics::NMSE(
                               const std::vector<double> &x,
                               const std::vector<double> &y
                               )
{
    // note: rather than compute means using the mean() function, keep track of sums and compute all means locally (this avoids extra divisions)
    
    auto N = x.size();
    
    double sse = 0.0;
    double sx  = 0.0;
    double sy  = 0.0;
    
    for(auto i = 0; i < N; ++i)
    {
        sse += ((x[i] - y[i])*(x[i] - y[i]));
        sx  += x[i];
        sy  += y[i];
    }
    
    return (N*sse/(sx*sy));
}
