// STL
#include <vector>                 // std::vector<>
// this
#include "statsxx/statistics.hpp" // sum()


// DESC:  Calculates the mean of x.

// NOTE: This ignores the issue of repeated roundoff error.
//
template<typename T>
inline double statistics::mean(
                               const std::vector<T> &x
                               )
{
    return ( static_cast<double>( sum(x) )/x.size() );
}

template<typename T, typename N>
inline double statistics::mean(
                               const T _T,
                               const N _N
                               )
{
    return ( static_cast<double>(_T) / static_cast<double>(_N) );
}
