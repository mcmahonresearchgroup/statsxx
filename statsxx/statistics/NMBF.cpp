/*
 Copyright 2013--Present JMM_PROGNAME
 
 This file is distributed under the terms of the JMM_PROGNAME License.
 
 You should have received a copy of the JMM_PROGNAME License.
 If not, see <JMM_PROGNAME WEBSITE>.
*/
// CREATED    : 10/17/2015
// LAST UPDATE: 11/24/2015


// STL
#include <vector>     // std::vector<>

// jScience
#include "jstats.hpp" // sum() << jmm: temp >>


//========================================================================
//========================================================================
//
// NAME: double statistics::NMBF(
//                               const std::vector<double> &x0,
//                               const std::vector<double> &x
//                               )
//
// DESC: Computes the normalized mean bias factor.
//
// INPUT:
//
//     std::vector<double> x0 : observations
//     std::vector<double> x  : model (results)
//
// NOTES:
//
//     - This is from:
//
//     S. Yu, et al. "New unbiased symmetric metrics for evaluation of air quality models", Atmospheric Science Letters 7, 26-–34 (2006)
//
//========================================================================
//========================================================================
inline double statistics::NMBF(
                               const std::vector<double> &x0,
                               const std::vector<double> &x
                               )
{
    // note: it is more efficient to call sum() than mean()
    double sumO = sum(x0);
    double sumM = sum(x);
    
    if(sumM >= sumO)
    {
        return (sumM/sumO - 1.0);
    }
    else
    {
        return (1.0 - sumO/sumM);
    }
}
