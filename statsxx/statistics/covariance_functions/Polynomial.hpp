
#ifndef STATSxx_STATISTICS_COVARIANCE_FUNCTIONS_POLYNOMIAL_HPP
#define STATSxx_STATISTICS_COVARIANCE_FUNCTIONS_POLYNOMIAL_HPP

// STL
#include <memory>    // std::unique_ptr<>
#include <vector>    // std::vector<>

// jScience
#include "jScience/linalg/Vector.hpp" // Vector<>


//=========================================================
// COVARIANCE FUNCTION
//=========================================================
class Polynomial : public CovarianceFunction
{
    
public:
    
    Polynomial(
               const int di,
               const std::vector<double> &th
               );
    ~Polynomial();
    
    std::unique_ptr<CovarianceFunction> clone() const;
    
    double              cov(
                            const Vector<double> &x1,
                            const Vector<double> &x2
                            );
    std::vector<double> ddtheta(
                                const Vector<double> &x1,
                                const Vector<double> &x2
                                );
    Vector<double>      ddx(
                            const Vector<double> &x,
                            const Vector<double> &x2
                            );
    
private:
    
    int d;
    
};

#include "statsxx/statistics/covariance_functions/src/Polynomial.cpp"


#endif
