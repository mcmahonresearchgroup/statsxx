#ifndef STATSxx_STATISTICS_COVARIANCE_FUNCTIONS_SQUAREDEXPONENTIAL_HPP
#define STATSxx_STATISTICS_COVARIANCE_FUNCTIONS_SQUAREDEXPONENTIAL_HPP

// STL
#include <memory>    // std::unique_ptr<>
#include <vector>    // std::vector<>

// Boost
#include <boost/serialization/export.hpp>
#include <boost/serialization/serialization.hpp> // boost::serialization::

// jScience
#include "jScience/linalg/Vector.hpp" // Vector<>


//=========================================================
// COVARIANCE FUNCTION
//=========================================================
class SquaredExponential : public CovarianceFunction
{

public:

    SquaredExponential();
    SquaredExponential(const std::vector<double> &th);
    ~SquaredExponential();

    std::unique_ptr<CovarianceFunction> clone() const;

    double              cov(const Vector<double> &x1, const Vector<double> &x2);
    std::vector<double> ddtheta(const Vector<double> &x1, const Vector<double> &x2);
    Vector<double>      ddx(const Vector<double> &x, const Vector<double> &x2);

private:

    // ----- (Boost) SERIALIZATION -----
    friend class boost::serialization::access;

    template<class Archive>
    void serialize(
                   Archive            &ar,
                   const unsigned int  version
                   )
    {
        ar & boost::serialization::base_object<CovarianceFunction>(*this);
    }

};

//BOOST_CLASS_EXPORT(SquaredExponential);

#include "statsxx/statistics/covariance_functions/src/SquaredExponential.cpp"


#endif
