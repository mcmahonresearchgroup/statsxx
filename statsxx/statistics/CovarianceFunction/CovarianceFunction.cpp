#include "../CovarianceFunction.hpp"

// STL
#include <stdexcept> // std::vector<>
#include <vector>    // std::vector<>

// jScience
#include "jScience/linalg/Vector.hpp" // Vector<>


inline CovarianceFunction::CovarianceFunction() {};

inline CovarianceFunction::CovarianceFunction(const std::vector<double> &th)
{
    this->theta = th;
}

inline CovarianceFunction::~CovarianceFunction() {};


//========================================================================
//========================================================================
//
// NAME: std::vector<double> CovarianceFunction::ddtheta(const Vector<double> &x1, const Vector<double> &x2)
//
// DESC: Default implementation of dk/dtheta (an error).
//
//========================================================================
//========================================================================
inline std::vector<double> CovarianceFunction::ddtheta(const Vector<double> &x1, const Vector<double> &x2)
{
    throw std::runtime_error("Error in CovarianceFunction::dk_dtheta(): function not implemented in derived class");
}


//========================================================================
//========================================================================
//
// NAME: Vector<double> CovarianceFunction::ddx(const Vector<double> &x, const Vector<double> &x2)
//
// DESC: Default implementation of dk/dx (an error).
//
//========================================================================
//========================================================================
inline Vector<double> CovarianceFunction::ddx(const Vector<double> &x, const Vector<double> &x2)
{
    throw std::runtime_error("Error in CovarianceFunction::dk_dx(): function not implemented in derived class");
}


//========================================================================
//========================================================================
//
// NAME: int CovarianceFunction::nhyperparameters()
//
// DESC: Returns the number of hyperparameters.
//
//========================================================================
//========================================================================
inline int CovarianceFunction::nhyperparameters()
{
    return this->theta.size();
}


//========================================================================
//========================================================================
//
// NAME: void CovarianceFunction::assign_hyperparameters(const std::vector<double> &th)
//
// DESC: Assigns the hyperparameters.
//
//========================================================================
//========================================================================
inline void CovarianceFunction::assign_hyperparameters(const std::vector<double> &th)
{
    this->theta = th;
}
