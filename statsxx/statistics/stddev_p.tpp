// STL
#include <cmath>                  // std::sqrt()
#include <vector>                 // std::vector<>

// this
#include "statsxx/statistics.hpp" // statistics::moment_p()


//
// DESC: Calculates the standard deviation of x.
//
template<typename T>
inline double statistics::stddev_p(
                                   const std::vector<T> &x
                                   )
{
    return std::sqrt(statistics::moment_p(2, x));
}
