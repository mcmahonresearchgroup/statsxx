// STL
#include <cmath>                  // std::pow()
#include <vector>                 // std::vector<>

// this
#include "statsxx/statistics.hpp" // statistics::moment_p(), statistics::stddev_p()


//
// DESC: Calculates the third standardized moment (skewness) of x.
//
template<typename T>
inline double statistics::skewness_p(
                                     const std::vector<T> &x
                                     )
{
    double gamma1;

    double mu3   = statistics::moment_p(
                                        3,
                                        x
                                        );

    double sigma = statistics::stddev_p(x);

    gamma1 = mu3/std::pow(sigma, 3);

    return gamma1;
}
