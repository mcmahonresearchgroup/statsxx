// STL
#include <vector>                  // std::vector<>

// jScience
#include "jScience/stl/vector.hpp" // std::vector<>+=
                                   //
                                   // NOTE: This allows for T == std::vector<>.


//
// DESC:  Sums x.
//
template<typename T>
inline T statistics::sum(
                         const std::vector<T> &x
                         )
{
    T sum;

    // TODO: NOTE: Could use the STL subroutine here:
    //
    //     std::accumulate( x.begin(), x.end(), 0.0 );

    if( x.size() > 0 )
    {
        sum = x[0];

        for( auto i = 1; i < x.size(); ++i )
        {
            sum += x[i];
        }
    }
    else
    {
        sum = T(0);
    }

    return sum;
}
