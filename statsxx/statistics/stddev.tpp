// STL
#include <cmath>                  // std::sqrt()
#include <vector>                 // std::vector<>
// this
#include "statsxx/statistics.hpp" // statistics::variance()


// DESC: Calculates the standard deviation of x.
//
template<typename T>
inline double statistics::stddev(
                                 const std::vector<T> &x
                                 )
{
    return std::sqrt(statistics::variance(x));
}

template<typename T, typename N>
inline double statistics::stddev(
                                 const T _T,
                                 const T _T2,
                                 const N _N
                                 )
{
    return std::sqrt(statistics::variance(_T, _T2, _N));
}
