// STL
#include <vector>                 // std::vector<>

// this
#include "statsxx/statistics.hpp" // statistics::moment_p()


//
// DESC: Calculates the fourth standardized moment (kurtosis) of x.
//
// NOTE: This is *excess kurtosis*.
//
template<typename T>
inline double statistics::kurtosis_p(
                                     const std::vector<T> &x
                                     )
{
    double gamma2;


    double mu2    = statistics::moment_p(
                                        2,
                                        x
                                        );

    double mu4    = statistics::moment_p(
                                        4,
                                        x
                                        );

    gamma2 = mu4/(mu2*mu2) - 3.;

    return gamma2;
}
