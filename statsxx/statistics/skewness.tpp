// STL
#include <cmath>                  // std::pow(), std::sqrt()
#include <vector>                 // std::vector<>
// this
#include "statsxx/statistics.hpp" // statistics::moment_p()


// DESC: Calculates the third *standardized* moment (skewness) of x.
//
// =====
//
// NOTE: The following uses the adjusted Fisher--Pearson standardized moment coefficient formula.
//
// NOTE: This "adjusted" formula is for sample size.
//
// NOTE: This adjustment has the smallest MSE (out of adjusted formulas) for asymmetric distributions ...
// NOTE: ... though, a higher MSE than for a normal distribution.
//
// NOTE: See:
//
//          D. N. Joanes and C. A. Gill, J. Royal Stat. Soc. (Series D): The Statistician. 47, 183--189 (1998).
//
template<typename T>
inline double statistics::skewness(
                                   const std::vector<T> &x
                                   )
{
    double G1;

    // NOTE: g1 (below) is defined in terms of biased estimates of the population moments.

    double m2 = statistics::moment_p(
                                     2,
                                     x
                                     );

    double m3 = statistics::moment_p(
                                     3,
                                     x
                                     );

    double g1 = m3/std::pow(m2, 1.5);

    G1 = (std::sqrt((x.size()*(x.size() - 1.)))/(x.size() - 2.))*g1;

    return G1;
}
