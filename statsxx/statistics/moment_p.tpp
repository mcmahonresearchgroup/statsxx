// STL
#include <cmath>                  // std::pow()
#include <vector>                 // std::vector<>

// this
#include "statsxx/statistics.hpp" // statistics::mean()


//
// DESC: Calculates the nth (central) moment of x.
//
// NOTE:
//
//     - The "zeroth" central moment mu_0 is 1.
//     - The first central moment mu_1 is 0.
//     - The second central moment mu_2 is the variance.
//     - The third and fourth central moments are used to define skewness and kurtosis, respectively.
//
template<typename T>
inline double statistics::moment_p(
                                   const int             n,
                                   const std::vector<T> &x
                                   )
{
    if( n == 0 )
    {
        return 1.;
    }
    else if( n == 1 )
    {
        return 0.;
    }

    //=========================================================

    double mu;

    double xmean = statistics::mean(x);

    double sum = 0.;

    for( auto &xi : x )
    {
        sum += std::pow((xi - xmean), n);
    }

    mu = sum/x.size();

    return mu;
}
