// STL
#include <cmath>                  // std::pow(), std::sqrt()
#include <vector>                 // std::vector<>
// this
#include "statsxx/statistics.hpp" // statistics::moment_p()


// DESC: Calculates the fourth *standardized* moment (kurtosis) of x, converted to *excess kurtosis*.
//
// =====
//
// NOTE: This "adjusted" formula is for sample size.
//
// NOTE: This adjustment has the smallest MSE (out of adjusted formulas) for asymmetric distributions ...
// NOTE: ... though, a higher MSE than for a normal distribution.
//
// NOTE: See:
//
//          D. N. Joanes and C. A. Gill, J. Royal Stat. Soc. (Series D): The Statistician. 47, 183--189 (1998).
//
template<typename T>
inline double statistics::kurtosis(
                                   const std::vector<T> &x
                                   )
{
    double G2;

    // NOTE: g2 (below) is defined in terms of biased estimates of the population moments.

    double m2 = statistics::moment_p(
                                     2,
                                     x
                                     );

    double m4 = statistics::moment_p(
                                     4,
                                     x
                                     );

    double g2 = m4/(m2*m2) - 3.;

    G2 = ((x.size() - 1.)/((x.size() - 2.)*(x.size() - 3.)))*((x.size() + 1.)*g2 + 6.);

    return G2;
}
