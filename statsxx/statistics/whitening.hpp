#ifndef STATSxx_STATISTICS_WHITENING_HPP
#define STATSxx_STATISTICS_WHITENING_HPP

// jScience
#include "jScience/linalg.hpp" // Matrix<>


namespace whitening
{
    Matrix<double> ZCAcor(
                          const Matrix<double> &Sigma
                          );
}

#include "statsxx/statistics/whitening/ZCAcor.cpp"

#endif
