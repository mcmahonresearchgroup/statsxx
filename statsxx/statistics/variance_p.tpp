// STL
#include <vector>                 // std::vector<>

// this
#include "statsxx/statistics.hpp" // statistics::moment_p()


//
// DESC: Calculates the variance of x.
//
// NOTE: This is simply a wrapper subroutine to statistics::moment_p().
//
template<typename T>
inline double statistics::variance_p(
                                     const std::vector<T> &x
                                     )
{
    return statistics::moment_p(2, x);
}
