// C++SL
#include <algorithm>  // std::transform()
#include <cmath>      // std::sqrt()
#include <functional> // std::multiplies<>()
#include <iterator>   // std::back_inserter()
#include <numeric>    // std::accumulate()
#include <vector>     // std::vector<>, ::begin(), ::end()


inline double statistics::wr(const std::vector<double>& w, const std::vector<double>& x, const std::vector<double>& y)
{
    // See: https://en.wikipedia.org/wiki/Pearson_correlation_coefficient#Weighted_correlation_coefficient

    //=============================================================================
    // sum_i w

    double wsum = std::accumulate(std::begin(w), std::end(w), double{ 0 });

    //=============================================================================
    // mx, my

    // sum_i w * x
    // sum_i w * y

    std::vector<double> wx;
    std::transform(std::begin(w), std::end(w), std::begin(x), std::back_inserter(wx), std::multiplies<double>());

    std::vector<double> wy;
    std::transform(std::begin(w), std::end(w), std::begin(y), std::back_inserter(wy), std::multiplies<double>());

    double wxsum = std::accumulate(std::begin(wx), std::end(wx), double{ 0 });
    double wysum = std::accumulate(std::begin(wy), std::end(wy), double{ 0 });

    // mx = sum_i wx * x / sum_i wy
    // my = sum_i wy * y / sum_i wy
    double mx = wxsum / wsum;
    double my = wysum / wsum;

    //=============================================================================
    // sx, sy, sxy

    double sxsum{ 0 };
    double sysum{ 0 };

    double sxysum{ 0 };

    auto it_w = std::begin(w);
    auto it_x = std::begin(x);
    auto it_y = std::begin(y);
    for (; (it_w != std::end(w)) && (it_x != std::end(x)) && (it_y != std::end(y)); ++it_w, ++it_x, ++it_y)
    {
        sxsum += ((*it_w) * (*it_x - mx) * (*it_x - mx));
        sysum += ((*it_w) * (*it_y - my) * (*it_y - my));

        sxysum += ((*it_w) * (*it_x - mx) * (*it_y - my));
    }

    double sx = sxsum / wsum;
    double sy = sysum / wsum;

    double sxy = sxysum / wsum;

    //=============================================================================
    // rho

    double rho = sxy / std::sqrt(sx * sy);

    //=============================================================================
    // RETURN

    return rho;
}
