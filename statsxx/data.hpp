#ifndef STATSxx_DATA_HPP
#define STATSxx_DATA_HPP

// STL
#include <string>              // std::string

// jScience
#include "jScience/linalg.hpp" // Matrix<>


Matrix<double> read_datafile(
                             const std::string filename
                             );
#include "statsxx/data/read_datafile.cpp"

void write_datafile(
                    const Matrix<double> &X,
                    const std::string     filename
                    );
#include "statsxx/data/write_datafile.cpp"


#endif
