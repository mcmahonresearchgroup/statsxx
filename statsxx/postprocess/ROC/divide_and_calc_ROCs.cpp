// STL
#include <vector>                      // std::vector<>

// jScience
#include "jrandnum.hpp"                // rand_num_uniform_Mersenne_twister()

// stats++
#include "statsxx/postprocess/ROC.hpp" // ROC_pt, calc_ROC_curve()


//
// DESC: Generated nROCs sets from set of test samples L and probabilities f, and calculates their ROC curves.
//
// INPUT:
//
//     L       : set of test examples
//     f(i)    : probabilistic classifier's estimate that example i is positive
//     nROCs   : number of ROCs to calculate
//
// NOTE: Bootstrapping is used to resample L and f to generate the nROCs sets.
//
// TODO: NOTE: ... Could add other methods (such as allow for test sets from cross validation, etc.).
//
inline std::vector<std::vector<ROC_pt>> divide_and_calc_ROCs(
                                                             const std::vector<int>    &L,
                                                             const std::vector<double> &f,
                                                             // -----
                                                             const int                  nROCs
                                                             )
{
    std::vector<std::vector<ROC_pt>> ROCs(nROCs);

    for(int i = 0; i < nROCs; ++i)
    {
        std::vector<int>    Lset;
        std::vector<double> fset;

        for(auto j = 0; j < L.size(); ++j)
        {
            int r = rand_num_uniform_Mersenne_twister( 0, (L.size()-1) );

            Lset.push_back(L[r]);
            fset.push_back(f[r]);
        }

        ROCs[i] = calc_ROC_curve(
                                 Lset,
                                 fset
                                 );


    }

    return ROCs;
}
