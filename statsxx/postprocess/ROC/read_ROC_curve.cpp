// STL
#include <stdexcept>                   // std::runtime_error()
#include <string>                      // std::string
#include <utility>                     // std::pair<>, std::make_pair()
#include <vector>                      // std::vector<>

// jScience
#include "jScience/linalg.hpp"         // Matrix<>

// stats++
#include "statsxx/data.hpp"            // read_datafile()
#include "statsxx/postprocess/ROC.hpp" // ROC_pt


//
// DESC: Input ROC curve.
//
// TODO: NOTE: There may be a more efficient way to input an ROC curve --- perhaps by overloading operators.
// TODO: NOTE: ... This would (likely) not work (straightforwardly) for the other output_ROC_curve() though (since both ROC and its stddev are output).
//
inline std::pair<
                 std::vector<ROC_pt>, // ROC
                 std::vector<ROC_pt>  // stddevROC
                 > read_ROC_curve(
                                  const std::string filename
                                  )
{
    std::vector<ROC_pt> ROC;
    std::vector<ROC_pt> stddevROC;

    Matrix<double> X  = read_datafile(
                                      filename
                                      );

    // if standard deviations exist ...
    if( X.size(1) == 5 )
    {
        for( auto i = 0; i < X.size(0); ++i )
        {
            ROC_pt pt;
            pt.FPR   = X(i,0);
            pt.TPR   = X(i,1);
            pt.score = X(i,4);

            ROC.push_back(pt);

            ROC_pt stddev_pt;
            stddev_pt.FPR   = X(i,2);
            stddev_pt.TPR   = X(i,3);
            stddev_pt.score = 0.;

            stddevROC.push_back(stddev_pt);
        }
    }
    // ... else they do not
    else if( X.size(1) == 3 )
    {
        for( auto i = 0; i < X.size(0); ++i )
        {
            ROC_pt pt;
            pt.FPR   = X(i,0);
            pt.TPR   = X(i,1);
            pt.score = X(i,2);

            ROC.push_back(pt);
        }
    }
    // ... else bad input
    else
    {
        throw std::runtime_error( ("error in read_ROC_curve(): data not formatted correctly in file = " + filename) );
    }

    return std::make_pair(
                          ROC,
                          stddevROC
                          );
}
