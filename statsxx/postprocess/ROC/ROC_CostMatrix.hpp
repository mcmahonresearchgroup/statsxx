#ifndef STATSxx_POSTPROCESS_ROC_ROC_CostMatrix_HPP
#define STATSxx_POSTPROCESS_ROC_ROC_CostMatrix_HPP


//
// NOTE: Operating costs do not take into account overhead, as they do not effect the operating point on the ROC curve.
//
// NOTE: Benefits can be interpreted as negative costs.
//
// TODO: NOTE: If wrapped in a namespace, this would (could) become ROC::CostMatrix.
//
typedef struct ROC_CostMatrix
{
    double P;  // probability of positive cases
               // NOTE: probability of negative cases = 1 - P
    double FP; // cost of a wrong prediction (the average loss per wrong prediction)
    double TN; // cost of identifying a negative prediction (correctly)
    double FN; // cost of missing a correct prediction
    double TP; // cost of predicting correctly --- in other words, the cost to make a prediction
} ROC_CostMatrix;


#endif
