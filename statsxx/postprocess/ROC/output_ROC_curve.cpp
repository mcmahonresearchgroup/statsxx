// STL
#include <fstream>                     // std::ofstream
#include <limits>                      // std::numeric_limits<>
#include <string>                      // std::string
#include <vector>                      // std::vector<>

// stats++
#include "statsxx/postprocess/ROC.hpp" // ROC_pt


//
// DESC: Output ROC curve.
//
// TODO: NOTE: There may be a more efficient way to output an ROC curve --- perhaps by overloading operators.
// TODO: NOTE: ... This would (likely) not work (straightforwardly) for the other output_ROC_curve() though (since both ROC and its stddev are output).
//
inline void output_ROC_curve(
                             const std::vector<ROC_pt> &ROC,
                             // -----
                             const std::string          filename
                             )
{
    std::ofstream ofs(filename);
    ofs.precision(std::numeric_limits<double>::max_digits10);

    for( auto pt : ROC )
    {
        ofs << pt.FPR << '\t' << pt.TPR << '\t' << pt.score << '\n';
    }

    ofs << '\n';

    ofs.close();
}


//
// DESC: Output ROC curve.
//
inline void output_ROC_curve(
                             const std::vector<ROC_pt> &ROC,
                             const std::vector<ROC_pt> &stddevROC,
                             // -----
                             const std::string          filename
                             )
{
    std::ofstream ofs(filename);
    ofs.precision(std::numeric_limits<double>::max_digits10);

    for( auto i = 0; i < ROC.size(); ++i )
    {
        ofs << ROC[i].FPR << '\t' << ROC[i].TPR << '\t' << stddevROC[i].FPR << '\t' << stddevROC[i].TPR << '\t' << ROC[i].score << '\n';
    }

    ofs << '\n';

    ofs.close();
}
