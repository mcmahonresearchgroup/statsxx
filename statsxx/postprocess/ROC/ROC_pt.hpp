#ifndef STATSxx_POSTPROCESS_ROC_ROC_PT_HPP
#define STATSxx_POSTPROCESS_ROC_ROC_PT_HPP


typedef struct ROC_pt
{
    double FPR;
    double TPR;
    double score;
} ROC_pt;


#endif
