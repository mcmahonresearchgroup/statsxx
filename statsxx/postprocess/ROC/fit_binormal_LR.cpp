// STL
#include <cmath>                       // std::sqrt()
#include <string>                      // std::string
#include <tuple>                       // std::tuple<>, ::make_tuple(), ::tie()
#include <vector>                      // std::vector<>

// stats++
#include "statsxx/postprocess/ROC.hpp" // ROC_pt, fit_binormal()


//
// DESC: Estimate (fit) an ROC curve to a binormal-LR model.
//
// INPUT:
//
//     std::vector<ROC_pt> ROC       :
//     std::vector<ROC_pt> stddevROC :
//
//     unsigned int        nROCs     :
//
// OUTPUT:
//
//     bool                success   :
//     std::string         msg       :
//     double              d_a       : Metz and Pan binormal-LR parameter
//     double              c         : Metz and Pan binormal-LR parameter
//
// -----
//
// NOTE: The binormal-LR parameters are (directly) calculable from the binormal parameters.
//
// -----
//
// NOTE: See:
//
//    C. E. Metz and X. C. Pan, ``"Proper" binormal ROC curves: theory and maximum-likelihood estimation,'' J. Math. Psych. 43, 1-33 (1999).
//
//    S. L. Hillis, ``Equivalence of binormal likelihood-ratio and bi-chi-squared ROC curve models,'' Stat. Med. 35, 2031--2057 (2016).
//
inline std::tuple<
                  bool,
                  std::string,
                  double,
                  double
                  > fit_binormal_LR(
                                    std::vector<ROC_pt> ROC,
                                    std::vector<ROC_pt> stddevROC,
                                    // -----
                                    const int           nROCs
                                    )
{
    bool                success;
    std::string         msg;
    double              d_a;
    double              c;

    //=========================================================
    // FIT CURVE TO BINORMAL MODEL
    //=========================================================

    double              a;
    double              b;
    std::tie(
             success,
             msg,
             a,
             b
             ) = fit_binormal(
                              ROC,
                              stddevROC,
                              // -----
                              nROCs
                              );

    //=========================================================
    // CALCULATE BINORMAL-LR FROM BINORMAL PARAMETERS
    //=========================================================
    //
    // NOTE: See Metz and Pan.
    // NOTE: ... Or/also Eqs. (23) and (24) in Harris.

    d_a = std::sqrt(2.)*a/std::sqrt(1. + b*b);

    c = (b - 1.)/(b + 1.);

    //=========================================================
    // RETURN
    //=========================================================

    return std::make_tuple(
                           true,
                           std::string(),
                           d_a,
                           c
                           );
}
