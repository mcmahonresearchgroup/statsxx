// STL
#include <vector>                      // std::vector<>

// function++
#include "functionsxx/integration.hpp" // functionsxx::integration::integrate()

// stats++
#include "statsxx/postprocess/ROC.hpp" // ROC_pt


//
// DESC: Calculate the area under an ROC curve, by numerical integration.
//
// INPUT:
//
//     std::vector<ROC_pt> ROC : ROC curve
//
// OUTPUT:
//
//     double              AUC :
//
// -----
//
// NOTE: For a good discussion of the probabilistic interpretation of AUC, see the following:
//
//     https://www.alexejgossmann.com/auc/
//
// -----
//
// NOTE: Testing on a binormal curve, this approach achieves an accuracy (for 1001 points) on the order of E-08.
//
inline double AUC_numerical(
                            const std::vector<ROC_pt> &ROC
                            )
{
    double AUC;

    std::vector<double> x;
    std::vector<double> y;

    for( auto i = 0; i < ROC.size(); ++i )
    {
        x.push_back(ROC[i].FPR);
        y.push_back(ROC[i].TPR);
    }

    AUC = functionsxx::integration::integrate(
                                              x,
                                              y
                                              );

    return AUC;
}
