// STL
#include <cmath>                               // std::sqrt()

// Boost
#include <boost/math/distributions/normal.hpp> // boost::math::cdf(), ::normal()


//
// DESC: Calculate the area under a binormal ROC curve.
//
// INPUT:
//
//     double       a : binormal coefficient
//     double       b : binormal coefficient
//
// OUTPUT:
//
//     double         : AUC
//
// NOTE: The area under a binormal ROC curve:
//
//     AUC = Phi(a/sqrt(1 + b*b))
//
//     where Phi is the distribution function of a standard normal variable.
//
inline double AUC_binormal(
                           const double a,
                           const double b
                           )
{
    return boost::math::cdf( boost::math::normal(), (a/std::sqrt(1. + b*b)) );
}







