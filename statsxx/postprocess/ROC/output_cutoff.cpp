// STL
#include <fstream> // std::ofstream
#include <limits>  // std::numeric_limits<>
#include <string>  // std::string


//
// DESC: Output the cutoff (optimal operating point) on an (ROC) curve to file.
//
// INPUT:
//
//     double      _cutoff  : cutoff
//     std::string filename : filename for output
//
// OUTPUT:
//
inline void output_cutoff(
                          const double      _cutoff,
                          // -----
                          const std::string filename
                          )
{
    std::ofstream ofs(filename);
    ofs.precision(std::numeric_limits<double>::max_digits10);

    ofs << "cutoff: " << _cutoff << '\n';

    ofs.close();
}







