// STL
#include <fstream> // std::ofstream
#include <limits>  // std::numeric_limits<>
#include <string>  // std::string


//
// DESC: Output the area under an (ROC) curve (AUC) to file.
//
// INPUT:
//
//     double      _AUC     : area under curve (AUC)
//     std::string filename : filename for output
//
// OUTPUT:
//
inline void output_AUC(
                       const double      _AUC,
                       // -----
                       const std::string filename
                       )
{
    std::ofstream ofs(filename);
    ofs.precision(std::numeric_limits<double>::max_digits10);

    ofs << "AUC: " << _AUC << '\n';

    ofs.close();
}







