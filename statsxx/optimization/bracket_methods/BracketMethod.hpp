/*
 Copyright 2013--Present JMM_PROGNAME
 
 This file is distributed under the terms of the JMM_PROGNAME License.
 
 You should have received a copy of the JMM_PROGNAME License.
 If not, see <JMM_PROGNAME WEBSITE>.
*/
// CREATED    : 7/15/2015
// LAST UPDATE: 7/16/2015

#ifndef STATSxx_OPTIMIZATION_BRACKET_METHODS_BRACKETMETHOD_HPP
#define STATSxx_OPTIMIZATION_BRACKET_METHODS_BRACKETMETHOD_HPP


// STL
#include <functional> // std::function<>
#include <tuple>      // std::tuple<>


//=========================================================
// BRACKET METHOD
//=========================================================
class BracketMethod
{
    
public:
    
    double max_stp;
    
    
    BracketMethod();
    ~BracketMethod();

    std::tuple<double,
               double,
               double,
               double,
               double,
               double> bracket(std::function<double(const double)> f,
                               double x1,
                               double x2);
 
};

#include "statsxx/optimization/bracket_methods/src/BracketMethod.cpp"


#endif	
