/*
 Copyright 2013--Present JMM_PROGNAME
 
 This file is distributed under the terms of the JMM_PROGNAME License.
 
 You should have received a copy of the JMM_PROGNAME License.
 If not, see <JMM_PROGNAME WEBSITE>.
*/
// CREATED    : 7/15/2015
// LAST UPDATE: 7/15/2015

#ifndef STATSxx_OPTIMIZATION_BRACKET_METHODS_BRENT_HPP
#define STATSxx_OPTIMIZATION_BRACKET_METHODS_BRENT_HPP


// STL
#include <functional> // std::function<>
#include <tuple>      // std::tuple<>

// stats++
#include "statsxx/optimization/bracket_methods/BracketMethod.hpp" // BracketMethod


//=========================================================
// BRENT'S METHOD
//=========================================================
class Brent : public BracketMethod
{
    
public:
    
    double tol;
    int iter_max;
    
    
    Brent();
    ~Brent();

    std::tuple<int,
               double,
               double> minimize(std::function<double(const double)> f,
                                double x1,
                                double x2);
    
};

#include "statsxx/optimization/bracket_methods/src/Brent.cpp"


#endif	
