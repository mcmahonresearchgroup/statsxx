#ifndef STATSxx_OPTIMIZATION_EA_FITNESS_HPP
#define STATSxx_OPTIMIZATION_EA_FITNESS_HPP


// STL
#include <functional>                             // std::function<>
#include <utility>                                // std::pair<>
#include <vector>                                 // std::vector<>

// this
#include "statsxx/optimization/EA/EAParam.hpp"    // EAParam
#include "statsxx/optimization/EA/Individual.hpp" // Individual
#include "statsxx/optimization/EA/Population.hpp" // Population


std::pair<
          std::vector<double>, // f
          std::vector<double>  // v
          > calc_fitness(
                         Population                                                            &population,
                         // =====
                         const std::function<double(const std::vector<double> &)>              &fitness,
                         // -----
                         const std::vector<std::function<double(const std::vector<double> &)>> &inequality_constraints,
                         const std::vector<std::function<double(const std::vector<double> &)>> &equality_constraints
                         );
#include "statsxx/optimization/EA/fitness/calc_fitness.cpp"


double calc_shared_fitness(
                           const Individual &indiv,
                           const Population &population,
                           // -----
                           const EAParam &param
                           );
#include "statsxx/optimization/EA/fitness/calc_shared_fitness.cpp"


#endif
