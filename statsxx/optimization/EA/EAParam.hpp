#ifndef STATSxx_OPTIMIZATION_EA_EAPARAM_HPP
#define STATSxx_OPTIMIZATION_EA_EAPARAM_HPP


// STL
#include <string> // std::string


//=========================================================
// EA PARAMETERS
//=========================================================
class EAParam
{

public:

    // POPULATION SIZE
    int    pop_size;

    // MAX. GENERATIONS
    int    max_gen;

    // GENOME SIZE
    int    genome_size;

    // FITNESS SHARING
    bool   fitness_sharing;
    double fitness_share_sigma;
    double fitness_share_alpha;

    // ELITISM
    double frac_elite;
    double frac_elite_feasible;

    // SELECTION (TOURNAMENT)
    int    tournament_nparticipants;

    // MUTATION (VS. CROSSOVER) FRACTION
    //
    // NOTE: This is the probability to mutate without crossover.
    //
    double prob_mutate_only;

    // CROSSOVER
    int    xover_nk; // number of crossover points, in k-point crossover

    // MUTATION
    //
    // NOTE: The following are the probability to mutate after crossover.
    //
    double prob_mutate;
    double prob_new_gene; // probability of a new gene (given that mutation WILL occur)

    // OUTPUT DIRECTORY
    std::string out_dir;


    EAParam();
    ~EAParam();

};

#include "statsxx/optimization/EA/EAParam/EAParam.cpp"


#endif
