// STL
#include <algorithm>  // std::max_element()
#include <functional> // std::function<>
#include <vector>     // std::vector<>

// stats++
#include "statsxx/statistics.hpp" // statistics::mean()

// this
#include "statsxx/optimization/EA/Population.hpp" // Population


//
// DESC: Calculate fitnesses (unshared).
//
// TODO: Rename, and assign avg and max outside.
//
inline void calc_fitness(
                         Population &population,
                         // -----
                         std::function<double(const std::vector<double> &)> fitness_func
                         )
{
    std::vector<double> fitnesses;
    for( auto &ind : population.individuals )
    {
        ind.fitness = fitness_func(ind.genome);
        fitnesses.push_back(ind.fitness);
    }

    population.avg_fitness = statistics::mean(fitnesses);
    population.max_fitness = *std::max_element(fitnesses.begin(), fitnesses.end());
}
