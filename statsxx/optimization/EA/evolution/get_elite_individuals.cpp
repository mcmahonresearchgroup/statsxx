// STL
#include <vector>                                 // std::vector<>

// jScience
#include "jutility.hpp"                           // sort_indices_desc()

// this
#include "statsxx/optimization/EA/Individual.hpp" // Individual
#include "statsxx/optimization/EA/Population.hpp" // Population


//
// DESC: Get n elite individuals from a population.
//
// =====
//
// NOTE: The return std::vector<> is sorted in descending order (of fitness).
//
inline std::vector<Individual> get_elite_individuals(
                                                     const int                  n,
                                                     // -----
                                                     const bool                 feasible,
                                                     // =====
                                                     const Population          &_population,
                                                     // -----
                                                     const std::vector<double> &f,
                                                     const std::vector<double> &v
                                                     )
{
    std::vector<Individual> elite;

    for( auto &idx : sort_indices_desc(f) )
    {
        if( feasible && (v[idx] == 0.) )
        {
            elite.push_back( _population.individuals[idx] );
        }
        else if( !feasible && (v[idx] != 0.) )
        {
            elite.push_back( _population.individuals[idx] );
        }

        if( elite.size() == n )
        {
            break;
        }
    }

    return elite;
}
