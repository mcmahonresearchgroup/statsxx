// STL
#include <vector> // std::vector<>


//
// DESC: Calculate the distance between two genomes.
//
// TODO: Needs to be user-implemented.
//
inline double calc_genome_distance(
                                   const std::vector<double> &genome1,
                                   const std::vector<double> &genome2
                                   )
{
    double d = 0.;

    for( decltype(genome1.size()) i = 0; i < genome1.size(); ++i )
    {
        d += (genome1[i] - genome2[i])*(genome1[i] - genome2[i]);
    }

    return d;
}

