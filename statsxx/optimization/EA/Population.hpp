#ifndef STATSxx_OPTIMIZATION_EA_POPULATION_HPP
#define STATSxx_OPTIMIZATION_EA_POPULATION_HPP


// STL
#include <vector> // std::vector<>

// this
#include "statsxx/optimization/EA/Individual.hpp" // Individual


//=========================================================
// POPULATION
//=========================================================
class Population
{

public:

    std::vector<Individual> individuals;

    double                  total_fitness;
    double                  avg_fitness;
    double                  max_fitness;


    Population();
    ~Population();

};

#include "statsxx/optimization/EA/Population/Population.cpp"


#endif
