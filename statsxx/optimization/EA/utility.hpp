#ifndef STATSxx_OPTIMIZATION_EA_UTILITY_HPP
#define STATSxx_OPTIMIZATION_EA_UTILITY_HPP


// STL
#include <vector> // std::vector<>

// this
#include "statsxx/optimization/EA/EAParam.hpp"    // EAParam
#include "statsxx/optimization/EA/Population.hpp" // Population


double calc_genome_distance(
                            const std::vector<double> &genome1,
                            const std::vector<double> &genome2
                            );
#include "statsxx/optimization/EA/utility/calc_genome_distance.cpp"


Individual tournament_select(
                             const Population &population,
                             const Population &offspring,
                             // -----
                             const EAParam &param
                             );
#include "statsxx/optimization/EA/utility/tournament_select.cpp"


#endif
