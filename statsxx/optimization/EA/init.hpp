#ifndef STATSxx_OPTIMIZATION_EA_INIT_HPP
#define STATSxx_OPTIMIZATION_EA_INIT_HPP


// this
#include "statsxx/optimization/EA/EAParam.hpp"    // EAParam
#include "statsxx/optimization/EA/Population.hpp" // Population


Population init_population(
                           std::function<std::vector<double>()>  new_individual,
                           // -----
                           const EAParam                        &param
                           );
#include "statsxx/optimization/EA/init/init_population.cpp"


#endif
