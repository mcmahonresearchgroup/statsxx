// STL
#include <functional>                             // std::function<>
#include <vector>                                 // std::vector<>

// this
#include "statsxx/optimization/EA/EAParam.hpp"    // EAParam
#include "statsxx/optimization/EA/Individual.hpp" // Individual
#include "statsxx/optimization/EA/Population.hpp" // Population


//
// DESC: Generate the initial population.
//
inline Population init_population(
                                  std::function<std::vector<double>()>  new_individual,
                                  // -----
                                  const EAParam                        &param
                                  )
{
    Population population;

    for( int i = 0; i < param.pop_size; ++i )
    {
        Individual individual;
        individual.genome = new_individual();

        population.individuals.push_back( individual );
    }

    return population;
}
