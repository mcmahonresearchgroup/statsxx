#ifndef STATSxx_OPTIMIZATION_EA_INDIVIDUAL_HPP
#define STATSxx_OPTIMIZATION_EA_INDIVIDUAL_HPP


// STL
#include <vector> // std::vector<>


//=========================================================
// INDIVIDUAL
//=========================================================
class Individual
{

public:

    // TODO: Needs to be custom type.
    std::vector<double> genome;

    double fitness;


    Individual();
    ~Individual();

};

#include "statsxx/optimization/EA/Individual/Individual.cpp"


#endif
