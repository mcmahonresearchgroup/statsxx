#include "statsxx/optimization/EA/EAParam.hpp" // EAParam


inline EAParam::EAParam()
{
    // NOTE: The following default values are discussed in the README (for EA).

    // MAX. GENERATIONS
    max_gen                  = 1000;

    // GENOME SIZE
    genome_size              = 2;

    // POPULATION SIZE
    pop_size                 = this->genome_size*2;

    // FITNESS SHARING
    fitness_sharing          = false;
    fitness_share_sigma      = 0.;
    fitness_share_alpha      = 1.;

    // ELITISM
    //
    // NOTE: frac_elite is used [in the code --- reproduce()] by multiplying by this->pop_size; it is therefore natural to also define it in this way (if one wants to specify a number).
    //
    frac_elite               = 2./this->pop_size;
    frac_elite_feasible      = 0.5;

    // SELECTION (TOURNAMENT)
    tournament_nparticipants = 2;

    // MUTATION (VS. CROSSOVER) FRACTION
    prob_mutate_only         = 0.2;

    // CROSSOVER
    xover_nk                 = 1;

    // MUTATION
    prob_mutate              = 1./this->genome_size;
    prob_new_gene            = 0.1;

    // OUTPUT DIRECTORY
    out_dir                  = "./EA/";
}

inline EAParam::~EAParam() {};
