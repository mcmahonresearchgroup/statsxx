/*
 Copyright 2013--Present JMM_PROGNAME
 
 This file is distributed under the terms of the JMM_PROGNAME License.
 
 You should have received a copy of the JMM_PROGNAME License.
 If not, see <JMM_PROGNAME WEBSITE>.
*/
// CREATED    : 7/20/2015
// LAST UPDATE: 7/20/2015

#ifndef STATSxx_OPTIMIZATION_GRADIENT_BASED_RPROP_HPP
#define STATSxx_OPTIMIZATION_GRADIENT_BASED_RPROP_HPP


// STL
#include <functional> // std::function<>
#include <string>     // std::string
#include <tuple>      // std::tuple<>

// jScience
#include "jScience/linalg/Vector.hpp" // Vector<>


//=========================================================
// RPROP MINIMIZATION
//=========================================================
class RPROP
{
    
public:
    
    double eta_p;
    double eta_m;
    double fdelta;
    double delta_min;
    double delta_max;
    
    double ftol;    
    int iter_max;

    
    RPROP();
    ~RPROP();

    std::tuple<int,
               Vector<double>,
               double> minimize(std::function<double(const Vector<double> &)> f,
                                std::function<Vector<double>(const Vector<double> &)> df,
                                Vector<double> x);
    
};

#include "statsxx/optimization/gradient_based/src/RPROP.cpp"


#endif	
