/*
 Copyright 2013--Present JMM_PROGNAME
 
 This file is distributed under the terms of the JMM_PROGNAME License.
 
 You should have received a copy of the JMM_PROGNAME License.
 If not, see <JMM_PROGNAME WEBSITE>.
*/
// CREATED    : 7/16/2015
// LAST UPDATE: 7/18/2015

#ifndef STATSxx_OPTIMIZATION_LINE_SEARCH_METHODS_CONJUGATEGRADIENT_HPP
#define STATSxx_OPTIMIZATION_LINE_SEARCH_METHODS_CONJUGATEGRADIENT_HPP


// STL
#include <functional> // std::function<>
#include <string>     // std::string
#include <tuple>      // std::tuple<>

// jScience
#include "jScience/linalg/Vector.hpp" // Vector<>

// stats++
#include "statsxx/optimization/line_search_methods/LineSearchMethod.hpp" // LineSearchMethod


//=========================================================
// CONJUGATE GRADIENT MINIMIZATION
//=========================================================
class ConjugateGradient : public LineSearchMethod
{
    
public:
    
    std::string method;
    double ftol;
    double gtol;
    double tol;
    int iter_max;
    
    
    ConjugateGradient();
    ~ConjugateGradient();

    std::tuple<int,
               Vector<double>,
               double> minimize(std::function<double(const Vector<double> &)> f,
                                std::function<Vector<double>(const Vector<double> &)> df,
                                Vector<double> x);
    
private:
    
    bool converged(const double f, const double fi, const Vector<double> &g, const Vector<double> &pi);
    
};

#include "statsxx/optimization/line_search_methods/src/ConjugateGradient.cpp"


#endif	
