/*
 Copyright 2013--Present JMM_PROGNAME
 
 This file is distributed under the terms of the JMM_PROGNAME License.
 
 You should have received a copy of the JMM_PROGNAME License.
 If not, see <JMM_PROGNAME WEBSITE>.
*/
// CREATED    : 7/16/2015
// LAST UPDATE: 7/17/2015

#ifndef STATSxx_OPTIMIZATION_LINE_SEARCH_METHODS_LINESEARCHMETHOD_HPP
#define STATSxx_OPTIMIZATION_LINE_SEARCH_METHODS_LINESEARCHMETHOD_HPP


// STL
#include <functional> // std::function<>
#include <tuple>      // std::tuple<>

// jSciene
#include "jScience/linalg/Vector.hpp" // Vector<>


//=========================================================
// LINE SEARCH METHOD
//=========================================================
class LineSearchMethod
{
    
public:
    
    // << jmm: default settings are used in the dependent minimization subroutines, because this should probably actually be more flexible in the sense that any 1D minimization subroutine could be used >>
    
    LineSearchMethod();
    ~LineSearchMethod();

    std::tuple<int,
               Vector<double>,
               double> line_minimize(std::function<double(const Vector<double> &)> f,
                                     const Vector<double> &x,
                                     const Vector<double> &d);
    
};

#include "statsxx/optimization/line_search_methods/src/LineSearchMethod.cpp"


#endif	
