/*
 Copyright 2013--Present JMM_PROGNAME
 
 This file is distributed under the terms of the JMM_PROGNAME License.
 
 You should have received a copy of the JMM_PROGNAME License.
 If not, see <JMM_PROGNAME WEBSITE>.
*/
// CREATED    : 9/23/2015
// LAST UPDATE: 9/23/2015

#ifndef STATSxx_OPTIMIZATION_LINE_SEARCH_METHODS_POWELL_HPP
#define STATSxx_OPTIMIZATION_LINE_SEARCH_METHODS_POWELL_HPP


// STL
#include <functional> // std::function<>
#include <tuple>      // std::tuple<>

// jScience
#include "jScience/linalg/Matrix.hpp" // Matrix<>
#include "jScience/linalg/Vector.hpp" // Vector<>


std::tuple<
           int,
           Vector<double>,
           double
           > Powell(
                    std::function<double(const Vector<double> &)> f,
                    Vector<double> p,
                    Matrix<double> Xi,
                    const int max_it,
                    const double ftol
                    );

#include "statsxx/optimization/line_search_methods/src/Powell.cpp"


#endif
