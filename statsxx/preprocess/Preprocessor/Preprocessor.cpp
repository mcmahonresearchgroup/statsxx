#include "statsxx/preprocess/Preprocessor.hpp"


// jScience
#include "jScience/linalg/Matrix.hpp" // Matrix<>


inline Preprocessor::Preprocessor()
{
    this->init = false;
}

inline Preprocessor::Preprocessor(
                                  const Matrix<double> &X
                                  )
{
    this->initialize(X);
}

inline Preprocessor::~Preprocessor() {};

