#include "statsxx/preprocess/Preprocessor.hpp"


//
// DESC: Returns specific statistics collected for the input data (min, max, etc.)
//
inline std::vector<double> Preprocessor::min() const
{
    return this->x_min;
}

inline std::vector<double> Preprocessor::max() const
{
    return this->x_max;
}

inline std::vector<double> Preprocessor::std_dev() const
{
    return this->x_s;
}
