#include "statsxx/preprocess/Preprocessor.hpp"


// jScience
#include "jScience/linalg/Matrix.hpp" // Vector<>, Matrix<>


//
// DESC: Denormalize data.
//
inline Vector<double> Preprocessor::denormalize_data(
                                                     Vector<double> x
                                                     ) const
{
    if(this->init)
    {

        for(auto i = 0; i < x.size(); ++i)
        {
            if(this->x_max[i] != this->x_min[i])
            {
                x(i) = x(i)*this->x_max_minus_min[i] + this->x_min[i];
            }
            else
            {
                x(i) = this->x_max[i];
            }
        }

    }

    return x;
}

inline Matrix<double> Preprocessor::denormalize_data(
                                                     Matrix<double> X
                                                     ) const
{
    if(this->init)
    {

        for( int i = 0; i < X.get_size(0); ++i )
        {
            for( int j = 0; j < X.get_size(1); ++j )
            {
                if( this->x_max[j] != this->x_min[j] )
                {
                    X(i,j) = X(i,j)*this->x_max_minus_min[j] + this->x_min[j];
                }
                else
                {
                    X(i,j) = this->x_max[j];
                }
            }
        }

    }

    return X;
}


