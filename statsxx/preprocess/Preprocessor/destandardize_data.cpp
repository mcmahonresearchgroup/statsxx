#include "statsxx/preprocess/Preprocessor.hpp"

// STL
#include <algorithm>  // std::minmax_element()
#include <vector>     // std::vector<>

// jScience
#include "jScience/linalg/Matrix.hpp" // Matrix<>
#include "jstats.hpp" // mean()


//
// DESC: (de-)Standardizes data to have zero mean and unit standard deviation.
//
inline Vector<double> Preprocessor::destandardize_data(
                                                       Vector<double> x
                                                       ) const
{
    if(this->init)
    {

        for(auto i = 0; i < x.size(); ++i)
        {
            // NOTE: See the comments in Preprocessor::standardize_data().

            if( this->x_max[i] != this->x_min[i] )
            {
                x(i) = x(i)*this->x_s[i] + this->x_mean[i];
            }
            else
            {
                x(i) = this->x_max[i];
            }
        }

    }

    return x;
}

inline Matrix<double> Preprocessor::destandardize_data(
                                                       Matrix<double> X
                                                       ) const
{
    if(this->init)
    {

        for( int i = 0; i < X.get_size(0); ++i )
        {
            for( int j = 0; j < X.get_size(1); ++j )
            {
                // NOTE: See the comments in Preprocessor::standardize_data().

                if( this->x_max[j] != this->x_min[j] )
                {
                    X(i,j) = X(i,j)*this->x_s[j] + this->x_mean[j];
                }
                else
                {
                    X(i,j) = this->x_max[j];
                }
            }
        }

    }

    return X;
}
