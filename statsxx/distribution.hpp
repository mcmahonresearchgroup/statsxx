#ifndef STATSxx_DISTRIBUTION_HPP
#define STATSxx_DISTRIBUTION_HPP

// STL
#include <utility>                   // std::pair<>
#include <vector>                    // std::vector<>

// jScience
#include "jScience/linalg.hpp"       // Vector<>, Matrix<>

// stats++
#include "distribution/binomial.hpp" // binomial


namespace distribution
{
    std::pair<
              std::vector<double>, // CDF_x
              std::vector<double>  // CDF_y
              > CDF(
                    const std::vector<double> &data
                    );

    std::pair<
              std::vector<double>, // PDF_x
              std::vector<double>  // PDF_y
              > PDF(
                    const std::vector<double> &data
                    );


    double Mahalanobis(
                       const Vector<double> &x,
                       // -----
                       const Vector<double> &mu,
                       const Matrix<double> &cov
                       );


    // Kolmogorov--Smirnov

    std::pair<
              double, // D
              double  // p-value
              > KS_two(
                       std::vector<double> data_1,
                       std::vector<double> data_2
                       );

    double P_KS(
                const double z
                );

    double Q_KS(
                const double z
                );

    // PERMUTATION TEST(S)

    double permutationTest(
                           const std::vector<int>& X_A,
                           const std::vector<int>& X_B,
                           // =====
                           const int n = 1000
                           );
}

#include "statsxx/distribution/CDF.cpp"
#include "statsxx/distribution/PDF.cpp"

#include "statsxx/distribution/Mahalanobis.cpp"

#include "statsxx/distribution/KS_two.cpp"
#include "statsxx/distribution/P_KS.cpp"
#include "statsxx/distribution/Q_KS.cpp"

#include "statsxx/distribution/permutationTest.cpp"

#endif
