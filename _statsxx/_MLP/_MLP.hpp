#ifndef STATSxx_MLP__MLP_HPP
#define STATSxx_MLP__MLP_HPP

// STL
#include <string>              // std::string
#include <vector>              // std::vector<>

// jScience
#include "jScience/linalg.hpp" // Matrix<>

// this
#include "statsxx/machine_learning/NeuralNet.hpp" // NEURAL_NET


NEURAL_NET create_MLP(
                      const bool              create,
                      // -----
                      const std::string       dbn_file,
                      // -----
                      const std::vector<int> &architecture,
                      const int               af_type,
                      const bool              is_classif,
                      // -----
                      const std::string       mlp_file
                      );


NEURAL_NET train_MLP(
                     NEURAL_NET            mlp,
                     // -----
                     const int             method,
                     //------
                     const int             nepoch_min,
                     const int             nepoch_max,
                     // -----
                     const bool            early_stopping,
                     // -----
                     const double          lr,
                     const double          lr_min,
                     const double          lr_max,
                     // -----
                     const double          momentum,
                     // -----
                     const double          weight_penalty,
                     // -----
                     const double          qrprop_u,
                     const double          qrprop_d,
                     // -----
                     const double          scg_lambda,
                     const double          scg_sigma,
                     const double          scg_convg_iterfrac,
                     const double          scg_rk_tol,
                     // -----
                     EAParam               EA_param,
                     // -----
                     const Matrix<double> &X_in,
                     const Matrix<double> &X_out,
                     // -----
                     const int             npts_per_batch,
                     // -----
                     const std::string     mlp_file
                     );


void test_MLP(
              NEURAL_NET                     &mlp,
              // -----
              const std::vector<std::string> &label,
              const Matrix<double>           &X_in,
              const Matrix<double>           &X_out,
              // -----
              const double                    cutoff,
              // -----
              const std::string               prefix
              );


#endif
