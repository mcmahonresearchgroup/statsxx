#ifndef STATSxx_MLP_INIT_HPP
#define STATSxx_MLP_INIT_HPP

// STL
#include <string> // std::string
#include <tuple>  // std::tuple<>
#include <vector> // std::vector<>

// stats++
#include "statsxx/optimization/EA.hpp" // EAParam


std::tuple<
           std::string,         // mlp_file
           // =====
           bool,                // create
           // -----
           bool,                // create_classif
           // -----
           std::vector<int>,    // create_architecture
           int,                 // create_af_type
           std::string,         // create_dbn_file
           // =====
           bool,                // train
           // -----
           int,                 // train_method
           // -----
           int,                 // train_nepoch_min
           int,                 // train_nepoch_max
           // -----
           bool,                // train_early_stopping
           // -----
           double,              // train_lr
           double,              // train_lr_min
           double,              // train_lr_max
           // -----
           double,              // train_momentum
           double,              // train_weight_penalty
           // -----
           double,              // train_qrprop_u
           double,              // train_qrprop_d
           // -----
           double,              // train_scg_lambda
           double,              // train_scg_sigma
           double,              // train_scg_convg_iterfrac
           double,              // train_scg_rk_tol
           // -----
           EAParam,             // EA_param
           // -----
           std::string,         // train_X_in_file
           std::string,         // train_X_out_file
           // -----
           int,                 // train_npts_per_batch
           // =====
           bool,                // test
           // -----
           std::string,         // test_label_file
           std::string,         // test_X_in_file
           std::string,         // test_X_out_file
           // -----
           double,              // test_cutoff
           // =====
           std::string          // prefix
           > read_param_file(
                             const std::string param_filename
                             );


#endif
