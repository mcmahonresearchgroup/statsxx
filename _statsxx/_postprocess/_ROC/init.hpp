#ifndef STATSxx_MLP_INIT_HPP
#define STATSxx_MLP_INIT_HPP

// STL
#include <string> // std::string
#include <tuple>  // std::tuple<>
#include <vector> // std::vector<>

// stats++
#include "statsxx/postprocess/ROC.hpp" // ROC_CostMatrix


std::tuple<
           std::string,         // ROC_file
           // =====
           bool,                // create
           // -----
           std::string,         // X_expected_file
           std::string,         // X_predicted_file
           // -----
           bool,                // thresh_avg
           int,                 // nthresh
           int,                 // ta_nROC
           // =====
           bool,                // fit
           // -----
           std::string,         // model
           // -----
           int,                 // fit_nROC
           // -----
           int,                 // model_npts
           // =====
           bool,                // cutoff
           // -----
           std::string,         // cutoff_method
           // -----
           ROC_CostMatrix,      // cost_matrix
           // =====
           std::string          // prefix
           > read_param_file(
                             const std::string param_filename
                             );


#endif
