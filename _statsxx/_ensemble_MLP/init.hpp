#ifndef STATSxx_ENSEMBLE_MLP_INIT_HPP
#define STATSxx_ENSEMBLE_MLP_INIT_HPP

// STL
#include <string> // std::string
#include <tuple>  // std::tuple<>


std::tuple<
           std::string,         // ensemble_file
           // =====
           // [mlp] BLOCK
           // -----
           std::string,         //  mlp_file
           // -----
           std::string,         //  filename_mlp_create
           // -----
           std::string,         //  mlp_train_X_in_file
           std::string,         //  mlp_train_X_out_file
           std::string,         //  filename_mlp_train
           // =====
           // [create] BLOCK
           // -----
           bool,                // create
           // -----
           int,                 // nmlp
           // -----
           bool,                // is_classif
           // -----
           int,                 // ni
           int,                 // no
           // =====
           // [create] BLOCK
           // -----
           bool,                // train
           // -----
           std::string,         //  train_X_in_file
           std::string,         //  train_X_out_file
           // -----
           std::string,         //  train_method
           // =====
           // [optimize] BLOCK
           // -----
           bool,                 // optimize
           // -----
           std::string,          // optimize_X_in_file
           std::string,          // optimize_X_out_file
           // -----
           std::string,          // optimize_method
           // -----
           double,               // Ensemble_gen_err_eps
           // -----
           int,                  // BMC_N
           // =====
           // [test] BLOCK
           // -----
           bool,                 // test
           // -----
           std::string,          // test_X_in_file
           std::string,          // test_X_out_file
           // -----
           double,               // cutoff
           // =====
           std::string           // prefix
           > read_param_file(
                             const std::string param_filename
                             );


#endif
