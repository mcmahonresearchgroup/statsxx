#ifndef STATSxx_DBN_INIT_HPP
#define STATSxx_DBN_INIT_HPP

// STL
#include <string> // std::string
#include <tuple>  // std::tuple<>
#include <vector> // std::vector<>


std::tuple<
           std::string,         // dbn_file
           // =====
           bool,                // create
           // -----
           std::vector<int>,    // create_nunits
           std::vector<int>,    // create_units_type
           // =====
           bool,                // train
           // -----
           std::string,         // train_X_file
           // -----
           int,                 // train_nepoch
           int,                 // train_npts_per_batch
           // -----
           int,                 // train_K_start
           int,                 // train_K_end
           double,              // train_K_rate
           // -----
           bool,                // train_sample_v
           bool,                // train_sample_hdata
           // -----
           double,              // train_lr
           std::vector<double>, // train_lr_dot
           std::vector<double>, // train_lr_adj
           double,              // train_lr_min
           double,              // train_lr_max
           // -----
           double,              // train_p_start
           double,              // train_p_end
           double,              // train_p_rate
           std::vector<double>, // train_p_dot
           std::vector<double>, // train_p_adj
           // -----
           double,              // train_w_penalty
           // -----
           int,                 // train_convg_nno_improvement
           double,              // train_convg_max_inc_max_w
           // =====
           bool,                // test
           // -----
           std::string,         // test_X_file
           std::string          // test_H_file
           > read_param_file(
                             const std::string param_filename
                             );


#endif
