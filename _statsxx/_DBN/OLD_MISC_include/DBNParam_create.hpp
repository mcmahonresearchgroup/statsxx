#ifndef STATSXX_DBNPARAM_H
#define EA_PARAM_H


// STL
#include <vector> // std::vector<>


//=========================================================
// DBN PARAMETERS (CREATE)
//=========================================================
class DBNParam_create
{
    
public:
    
    std::vector<int>    nunits;      
    std::vector<int>    units_type;   
    
    
    DBNParam_create() {}; // no defaults
    ~DBNParam_create();
    
};


#endif	
