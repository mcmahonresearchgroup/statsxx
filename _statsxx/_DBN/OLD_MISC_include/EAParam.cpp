/*
 Copyright 2013--Present JMM_PROGNAME
 
 This file is distributed under the terms of the JMM_PROGNAME License.
 
 You should have received a copy of the JMM_PROGNAME License.
 If not, see <JMM_PROGNAME WEBSITE>.
*/
// CREATED    : 11/7/2013
// LAST UPDATE: 11/9/2013

#include "EAParam.hpp"


EAParam::EAParam()
{
    pop_size                 = 200;
    
    max_gen                  = 200;
    
    genome_size              = 0;
    
    fitness_sharing          = false;
    fitness_share_sigma      = 0.0;
    fitness_share_alpha      = 1.0;
    
    frac_elite               = 0.01;
    tournament_nparticipants = 2;
    
    xover_nk                 = 2;
    
    prob_mutate_only         = 0.25;
    prob_mutate              = 0.02;
    prob_new_gene            = 0.01;
    
    out_dir                  = "./EA/";
}


EAParam::~EAParam() {};
