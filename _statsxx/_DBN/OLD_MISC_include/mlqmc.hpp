#ifndef MLQMC_MLQMC_INCLUDE_MLQMC_HPP
#define MLQMC_MLQMC_INCLUDE_MLQMC_HPP

// STL
#include <string>  // std::string
#include <utility> // std::pair<>

// jScience
#include "jScience/linalg.hpp"      // Matrix<double>


std::pair<
          Matrix<double>,
          Matrix<double>
          > generate_dataset(
                             const std::string filename_in,
                             const std::string filename_out
                             );

Matrix<double> read_file(
                         const std::string filename
                         );


#endif
