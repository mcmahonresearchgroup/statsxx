// STL
#include <fstream> // std::ofstream
#include <string>  // std::string
#include <vector>  // std::vector<>

// jScience
#include "jScience/linalg.hpp" // Matrix<>

// stats++
#include "statsxx/machine_learning/DBN.hpp" // machine_learning::DBN


//
// DESC: Tests a DBN.
//
void test_DBN(
              const machine_learning::DBN &dbn,
              // -----
              const Matrix<double>        &X,
              // -----
              const std::string            H_file
              )
{
    std::ofstream ofs(H_file);
    
    std::vector<Matrix<double>> H = dbn.v_to_h(
                                               X
                                               );
    
    for(auto i = 0; i < X.size(0); ++i)
    {
        // OUTPUT ALL HIDDEN NEURONS
        for(auto k = 0; k < H.size(); ++k)
        {
            for(auto j = 0; j < H[k].size(1); ++j)
            {
                ofs << H[k](i,j) << " ";
            }
        }
        
//        for(auto j = 0; j < X_out.size(1); ++j)
//        {
//            ofs << X_out(i,j) << " ";
//        }
        
        ofs << '\n';
    }
    
    ofs.close();   
}
