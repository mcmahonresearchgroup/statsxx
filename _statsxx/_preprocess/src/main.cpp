// TODO: I wonder if this would be better if there could be the creation of a preprocessor (perhaps with an input file like the DBN), then it could be used in other calls to preprocess data
// TODO: ... this would be more efficient, because the preprocessor here has to be generated on every use


// STL
#include <cstdlib> // std::atoi()
#include <string>  // std::string

// jSciencerm
#include "jScience/linalg.hpp" // Matrix<>

// stats++
#include "statsxx/data.hpp"       // read_datafile(), write_datafile()
#include "statsxx/preprocess.hpp" // Preprocessor


//
// USAGE: preprocess filename_X(pp) filename_X(to_pp) pp_type filename_out
//
//     filename_X(pp)    :: data filename (to create preprocessor)
//     filename_X(to_pp) :: data filename (to preprocess)
//     pp_type           :: preprocessing type
//                          -2 == denormalize
//                          -1 == destandardize
//                           0 == nothing
//                           1 == standardize
//                           2 == normalize
//     filename_out      :: data filename (output)
//
int main(
         int argc,
         char* argv[]
         )
{
    //=========================================================
    // INITIALIZATION
    //=========================================================

    std::string filename_X_pp    = std::string(argv[1]);
    std::string filename_X_to_pp = std::string(argv[2]);
    int         pp_type          = std::atoi(argv[3]);
    std::string filename_X_out   = std::string(argv[4]);

    //=========================================================
    // READ DATA
    //=========================================================

    Matrix<double> X_pp = read_datafile(
                                        filename_X_pp
                                        );

    Matrix<double> X_to_pp = read_datafile(
                                           filename_X_to_pp
                                           );

    //=========================================================
    // PREPROCESS DATA
    //=========================================================

    Preprocessor pp(X_pp);

    switch(pp_type)
    {
        case -2:
            X_to_pp = pp.denormalize_data(X_to_pp);
            break;
        case -1:
            X_to_pp = pp.destandardize_data(X_to_pp);
            break;
        case 0:
            break;
        case 1:
            X_to_pp = pp.standardize_data(X_to_pp);
            break;
        case 2:
            X_to_pp = pp.normalize_data(X_to_pp);
            break;
        default:
            break;
    }

    //=========================================================
    // WRITE DATA
    //=========================================================

    write_datafile(
                   X_to_pp,
                   filename_X_out
                   );

    return 0;
}

