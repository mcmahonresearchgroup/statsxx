#####
# INPUT FILE FOR stats++ GP
#####

# NOTE: This file trains a Gaussian process.

#####

# NOTE: gp_file is always required.
gp_file        = gp.txt

# NOTE: prefix is always required.
prefix         = gp

[train]

# NOTE: noise parameter used for training.
sigma_n        = 1.e-8

# NOTE: noise parameter used for training.
optimize_hyperparameters = true

X_in_file      = train_X_in.dat 
x_out_file     = train_x_out.dat

