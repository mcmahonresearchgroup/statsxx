// STL
#include <fstream>             // std::ofstream
#include <limits>              // std::numeric_limits<>
#include <string>              // std::string
// jScience
#include "jScience/linalg.hpp" // Matrix<>, Vector<>

// stats++
#include "statsxx/machine_learning/Gaussian_process.hpp" // Gaussian process stuff


//
// DESC: Tests a GP.
//
void test_GP(
              std::unique_ptr<Gaussian_process::GaussianProcess>  gp,
              // -----
              const Matrix<double>                               &X_in,
              const Vector<double>                               &x_out,
              // -----
              const std::string                                   prefix
              )
{
    std::ofstream ofs(("./" + prefix + ".test.out.dat"));
    ofs.precision(std::numeric_limits<double>::max_digits10);

    for(auto i = 0; i < X_in.size(0); ++i)
    {
        // PREDICT
        double pred = gp->predict(
                                  X_in.row(i)
                                  );

        // OUTPUT
        ofs << pred << '\n';
    }

    ofs.close();
}
