#ifndef STATSxx_MLP_INIT_HPP
#define STATSxx_MLP_INIT_HPP

// STL
#include <string> // std::string
#include <tuple>  // std::tuple<>


std::tuple<
           std::string, // gp_file
           // =====
           bool,        // create
           // -----
           int,         // ni
           int,         // no
           // -----
           std::string, // covariance_function
           double,      // theta
           // =====
           bool,        // train
           // -----
           double,      // sigma_n
           // -----
           bool,        // optimize_hyperparameters
           // -----
           std::string, // train_X_in_file
           std::string, // train_x_out_file
           // =====
           bool,        // test
           // -----
           std::string, // test_X_in_file
           std::string, // test_x_out_file
           // =====
           std::string  // prefix
           > read_param_file(
                             const std::string param_filename
                             );


#endif
