#ifndef STATSxx_GP__GP_HPP
#define STATSxx_GP__GP_HPP

// STL
#include <string>              // std::string
#include <memory>              // std::unique_ptr<>

// jScience
#include "jScience/linalg.hpp" // Matrix<>, Vector<>

// stats++
#include "statsxx/machine_learning/Gaussian_process.hpp" // Gaussian process stuff


std::unique_ptr<Gaussian_process::GaussianProcess> create_GP(
                                                             const bool        create,
                                                             // -----
                                                             const int         ni,
                                                             const int         no,
                                                             // -----
                                                             const std::string covariance_function,
                                                             const double      _theta,
                                                             // -----
                                                             const std::string gp_file
                                                             );


std::unique_ptr<Gaussian_process::GaussianProcess> train_GP(
                                                            std::unique_ptr<Gaussian_process::GaussianProcess>  gp,
                                                            // -----
                                                            double                                              sigma_n,
                                                            // -----
                                                            const bool                                          optimize_hyperparameters,
                                                            //------
                                                            const Matrix<double>                               &X_in,
                                                            const Vector<double>                               &x_out,
                                                            // -----
                                                            const std::string                                   gp_file
                                                            );


void test_GP(
              std::unique_ptr<Gaussian_process::GaussianProcess>  gp,
              // -----
              const Matrix<double>                               &X_in,
              const Vector<double>                               &x_out,
              // -----
              const std::string                                   prefix
              );


#endif
