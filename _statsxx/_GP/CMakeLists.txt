#cmake_minimum_required(VERSION 3.3.1)

project(gp)

set(CMAKE_BUILD_TYPE Release)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")
if(APPLE)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -framework Accelerate")
endif()
 
#---------------------------------------------------------
# COMPILATION
#---------------------------------------------------------

include_directories(.)

# add_sources
macro (add_sources)
    file (RELATIVE_PATH _relPath "${CMAKE_SOURCE_DIR}" "${CMAKE_CURRENT_SOURCE_DIR}")
    foreach (_src ${ARGN})
        if (_relPath)
            list (APPEND SRCS "${_relPath}/${_src}")
        else(_relPath)
            list (APPEND SRCS "${_src}")
        endif(_relPath)
    endforeach(_src ${ARGN})
    if (_relPath)
        # propagate SRCS to parent directory
        set (SRCS ${SRCS} PARENT_SCOPE)
    endif(_relPath)
endmacro (add_sources)

add_subdirectory(_GP)
add_subdirectory(init)
add_subdirectory(src)

add_executable(gp ${SRCS})

target_link_libraries(gp jscience)
# ---
# normal:
target_link_libraries(gp boost_program_options boost_serialization)
# ---
# Kamiak:
#target_link_libraries(mlp /data/cas/mcmahon/Boost/intel/boost_1_60_0/stage/lib/libboost_program_options.a)
#target_link_libraries(mlp /data/cas/mcmahon/Boost/intel/boost_1_60_0/stage/lib/libboost_serialization.a)
if(UNIX AND NOT APPLE)
    # normal:
    target_link_libraries(gp lapack blas)
    # ---
    # Kamiak:
#    target_link_libraries(mlp /usr/lib64/liblapack.so.3.4.2)
endif()

#---------------------------------------------------------
# INSTALL
#---------------------------------------------------------

# default install to /usr/local/bin
#set (CMAKE_INSTALL_PREFIX ".." CACHE PATH "default install path" FORCE )
#install(TARGETS dbn DESTINATION bin)
