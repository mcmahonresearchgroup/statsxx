// STL
#include <fstream>                  // std::ofstream
#include <limits>                   // std::numeric_limits<>::
#include <string>                   // std::string
#include <tuple>                    // std::tie()
#include <vector>                   // std::vector<>

// jScience
#include "jScience/stl/istream.hpp" // read_file()

// stats++
#include "statsxx/distribution.hpp" // distribution::PDF


//
// DESC: Estimate the probability density function (PDF) of the probability distribution from which a set of data was drawn.
//
// USAGE: PDF filename_in filename_out
//
//     filename_in   :: input filename
//     filename_out  :: output filename
//
int main(
         int argc,
         char* argv[]
         )
{
    //=========================================================
    // INITIALIZATION
    //=========================================================

    std::string filename_in = std::string(argv[1]);
    std::string filename_out = std::string(argv[2]);

    //=========================================================
    // READ DATA
    //=========================================================

    std::vector<double> data = read_file<double>(
                                                 filename_in
                                                 );

    //=========================================================
    // ESTIMATE PDF
    //=========================================================

    std::vector<double> PDF_x;
    std::vector<double> PDF_y;
    std::tie(
             PDF_x,
             PDF_y
             ) = distribution::PDF(
                                   data
                                   );

    //=========================================================
    // OUTPUT
    //=========================================================

    std::ofstream ofs(filename_out);
    ofs.precision(std::numeric_limits<double>::max_digits10);

    for( auto i = 0; i < PDF_x.size(); ++i )
    {
        ofs << PDF_x[i] << " " << PDF_y[i] << '\n';
    }

    ofs.close();

    //=========================================================
    // CLEANUP AND RETURN (EXIT)
    //=========================================================

    return 0;
}
