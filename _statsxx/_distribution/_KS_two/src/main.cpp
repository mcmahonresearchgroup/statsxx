// STL
#include <iostream>                 // std::cout
#include <limits>                   // std::numeric_limits<>::
#include <string>                   // std::string
#include <tuple>                    // std::tie()
#include <vector>                   // std::vector<>

// jScience
#include "jScience/stl/istream.hpp" // read_file()

// stats++
#include "statsxx/distribution.hpp" // distribution::Q_KS


//
// DESC: Perform the Kolmogorov--Smirnov test.
//
// USAGE: KS_two filename_data_1 filename_data_2
//
//     filename_data_1   :: data 1 filename
//     filename_data_2   :: data 2 filename
//
int main(
         int argc,
         char* argv[]
         )
{
    //=========================================================
    // INITIALIZATION
    //=========================================================

    std::string filename_data_1 = std::string(argv[1]);
    std::string filename_data_2 = std::string(argv[2]);

    //=========================================================
    // READ DATA
    //=========================================================

    std::vector<double> data_1 = read_file<double>(
                                                   filename_data_1
                                                   );

    std::vector<double> data_2 = read_file<double>(
                                                   filename_data_2
                                                   );

    //=========================================================
    // PERFORM TEST
    //=========================================================

    double D;
    double p;
    std::tie(
             D,
             p
             ) = distribution::KS_two(
                                      data_1,
                                      data_2
                                      );

    //=========================================================
    // OUTPUT
    //=========================================================

    std::cout.precision(std::numeric_limits<double>::max_digits10);

    std::cout << "D: " << D << '\n';
    std::cout << "p: " << p << '\n';

    //=========================================================
    // CLEANUP AND RETURN (EXIT)
    //=========================================================

    return 0;
}
