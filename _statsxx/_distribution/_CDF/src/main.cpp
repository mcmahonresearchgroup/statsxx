// STL
#include <fstream>                  // std::ofstream
#include <limits>                   // std::numeric_limits<>::
#include <string>                   // std::string
#include <tuple>                    // std::tie()
#include <vector>                   // std::vector<>

// jScience
#include "jScience/stl/istream.hpp" // read_file()

// stats++
#include "statsxx/distribution.hpp" // distribution::CDF


//
// DESC: Estimate the cumulative distribution function (CDF) of the probability distribution from which a set of data was drawn.
//
// USAGE: CDF filename_in filename_out
//
//     filename_in   :: input filename
//     filename_out  :: output filename
//
int main(
         int argc,
         char* argv[]
         )
{
    //=========================================================
    // INITIALIZATION
    //=========================================================

    std::string filename_in = std::string(argv[1]);
    std::string filename_out = std::string(argv[2]);

    //=========================================================
    // READ DATA
    //=========================================================

    std::vector<double> data = read_file<double>(
                                                 filename_in
                                                 );

    //=========================================================
    // ESTIMATE CDF
    //=========================================================

    std::vector<double> CDF_x;
    std::vector<double> CDF_y;
    std::tie(
             CDF_x,
             CDF_y
             ) = distribution::CDF(
                                   data
                                   );

    //=========================================================
    // OUTPUT
    //=========================================================

    std::ofstream ofs(filename_out);
    ofs.precision(std::numeric_limits<double>::max_digits10);

    for( auto i = 0; i < CDF_x.size(); ++i )
    {
        ofs << CDF_x[i] << " " << CDF_y[i] << '\n';
    }

    ofs.close();

    //=========================================================
    // CLEANUP AND RETURN (EXIT)
    //=========================================================

    return 0;
}
