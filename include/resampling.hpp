/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 3/16/2015
// LAST UPDATE: 3/16/2015

#ifndef RESAMPLING_H
#define RESAMPLING_H


// STL
#include <utility> // std::pair<>
#include <vector>  // std::vector<>


namespace statsxx
{
    namespace resampling
    {
        std::pair<std::vector<unsigned int>, std::vector<unsigned int>> kfold_indices( const unsigned int n1, const unsigned int n2, const unsigned int nk );
        std::pair<unsigned int, unsigned int> kfold_indices( const unsigned int n1, const unsigned int n2, const unsigned int nk, const unsigned int k );
    }
}


#endif
