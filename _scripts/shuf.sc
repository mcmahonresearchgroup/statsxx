#!/bin/bash

#=========================================================

labels=$1
in=$2
out=$3

#=========================================================

# PASTE FILES TOGETHER
paste -d : $1 $2 $3 > tmp.txt.dat

#=========================================================

# SHUFFLE
shuf tmp.txt.dat > shuffled.tmp.txt.dat

#=========================================================

# SETUP OUTPUT FILENAMES
labels_out="rand."$labels
in_out="rand."$in
out_out="rand."$out

#=========================================================

# EXPORT SHUFFLED DATA TO FILES
#awk -v FS=":" '{ print $1 > $labels_out ; print $2 > $in_out ; print $2 > $out_out }' shuffled.tmp.txt.dat

awk -v FS=":" '{print $1}' shuffled.tmp.txt.dat > $labels_out
awk -v FS=":" '{print $2}' shuffled.tmp.txt.dat > $in_out
awk -v FS=":" '{print $3}' shuffled.tmp.txt.dat > $out_out

#=========================================================

# CLEANUP
rm tmp.txt.dat
rm shuffled.tmp.txt.dat
