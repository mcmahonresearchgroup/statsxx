#!/bin/bash

# DESC: Setup file for k-fold cross validation. 
#
# USAGE: kfold_CV_setup.sc k labels_file > output_file
#
#     k           :: 
#     labels_file ::
#
#     output_file :: 

#=========================================================

# =====
# PARAMETERS
# =====

# NOTE: These are read from the command line.

k=$1
labels_file=$2

#=========================================================

# =====
# NUMBER OF LINES, DIVISION, ETC.
# =====

# FIND NUMBER OF LINES IN DATA FILE
nlines=$(wc -l < $labels_file)

# echo "nlines is = " $nlines

# FIND DIVISION SIZE AND REMAINDER INTO k FOLDS
div=$(expr $nlines / $k)
rem=$(expr $nlines % $k)

# echo "div is = " $div
# echo "rem is = " $rem

# =====
# LOOP
# =====

pln_e=0

n=0
while [ $n -lt $k ]; do

#     echo $n

    # FETCH LINE NUMBERS
    ln_s=`echo "($pln_e + 1)" | bc -l`
    ln_e=`echo "($ln_s + $div - 1)" | bc -l`

    if [ $n -lt $rem ]
    then

        let ln_e=ln_e+1 

    fi

    # OUTPUT LINE NUMBERS OF FOLD
    echo $ln_s $ln_e

    # UPDATE (PREVIOUS) LINE END
    let pln_e=$ln_e

    # UPDATE COUNTER
    let n=n+1 

done

